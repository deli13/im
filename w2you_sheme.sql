 drop database w2you;
create database w2you;
use w2you;

create table t_user(id_user int auto_increment primary key, login_user varchar(100) not null unique, role varchar(100), password_user varchar(40) not null, created_user timestamp default current_timestamp);

create table t_template(id_template int auto_increment primary key, name_template varchar(200) unique, path_template varchar(2000));

create table t_category (id_category int primary key auto_increment, name_category varchar(200) not null unique,description_category text,
created_category timestamp default current_timestamp, id_template int, updated_category timestamp, title_category varchar(2000), descr_category varchar(2000), keywords_category varchar(2000),
foreign key(id_template) references t_template(id_template)); -- таблица категорий услуги, новости, и.т.п.

create table t_brand (id_brand int primary key auto_increment, name_brand varchar(2000) not null, description_brand text, img_brand varchar(2000),
created_brand timestamp default current_timestamp default current_timestamp, updated_brand timestamp, title_brand varchar(2000), descr_brand varchar(2000), keywords_brand varchar(2000)); -- Таблица с инфо о cms для универсальности названа brand

create table t_img(id_img int primary key auto_increment, img_desktop varchar(2000), img_tablet_vert varchar(2000), img_tablet_horiz varchar(2000),
img_phone_vert varchar(2000), img_phone_horiz varchar(2000)); -- таблица с картиночками

create table t_publication(id_publication int primary key auto_increment, name_publication varchar(200) not null unique, description text,
id_category int, id_brand int, created_publication timestamp default current_timestamp, updated_publication timestamp, otziv_publication varchar(2000),
id_img int unique, price int, cpu varchar(2000), title_publication varchar(2000), descr_publication varchar(2000), keywords_publication varchar(2000),
foreign key(id_category) references t_category(id_category) on delete restrict,
foreign key(id_brand) references t_brand(id_brand) on delete restrict,
foreign key(id_img) references t_img(id_img)); -- Таблица с инфо о продукте(сайте)

create table t_content (id_content int primary key auto_increment, name_content varchar(2000), text_content text, img_content varchar(2000),
created_content timestamp default current_timestamp, updated_content timestamp, id_publication int,
foreign key(id_publication) references t_publication(id_publication)); -- таблица с контентом

create table t_news(id_news int primary key auto_increment, name_news varchar(2000), id_content int, created_news timestamp default current_timestamp,
updated_news timestamp , cpu varchar(2000), title_news varchar(2000), descr_news varchar(2000), keywords_news varchar(2000),
foreign key(id_content) references t_content(id_content) on delete restrict); -- таблица с новостями (новости так то у всех есть=^____^=)

create table t_hashtag(id_hashtag int auto_increment primary key, hashtag varchar(100)); -- хэштеги

create table t_hashtag_publication(id_hashtag_publication int primary key auto_increment, id_hashtag int, id_publication int,
foreign key (id_hashtag) references t_hashtag(id_hashtag),
foreign key (id_publication) references t_publication(id_publication)); -- связь хэштегов с публикациями

create table t_catalog(id_catalog int primary key auto_increment, name_catalog varchar(2000) not null, parent_catalog int, cpu_catalog varchar(2000),
text_catalog text, title_catalog varchar(2000), description_catalog varchar(2000), keywords_catalog varchar(2000));

create table t_сharaсter(id_character int primary key auto_increment, name_character varchar(2000) not null, ed_izm varchar(100), id_catalog int not null,
foreign key (id_catalog) references t_catalog(id_catalog));

create table t_product(id_product int primary key auto_increment, name_product varchar(2000) not null, description1_product text, description2_product text, description3_product text, url_public_product varchar(2000), cpu_product varchar(2000),
create_product timestamp default current_timestamp, update_product timestamp, id_user int not null, id_catalog int not null, price_product int, fake_price_product int,
title_product varchar(2000), description_product varchar(2000), keywords_product varchar(2000),
foreign key(id_catalog) references t_catalog(id_catalog),
foreign key (id_user) references t_user(id_user) );

create table t_character_product(id_character_product int primary key auto_increment, value_character_product varchar(200), id_product int not null, id_character int not null,
foreign key (id_product) references t_product(id_product),
foreign key (id_character) references t_сharaсter(id_character));

create table t_img_product(id_img_product int auto_increment primary key, img_product varchar(2000), id_product int not null,
foreign key(id_product) references t_product(id_product));

create table t_service(id_service int primary key auto_increment, name_service varchar(2000) not null,
text_service text, img_service varchar(2000) not null, price_service int, fake_service int, 
create_service timestamp default current_timestamp, update_service timestamp, title_service varchar(2000),
desccription_service varchar(2000), keywords_service varchar(2000),
id_user int, foreign key (id_user) references t_user(id_user));

alter table t_product add column description4_product text;
alter table t_product add column manual_product text;
alter table t_product add column viv_main boolean;
alter table t_product add column required_product varchar(200);
alter table t_product add column personal_product varchar(200);
alter table t_product add column service_product varchar(200);

alter table t_service add column cpu_service varchar(2000);

alter table t_catalog add column img_catalog varchar(2000);

alter table t_content add column out_header boolean;
alter table t_content add column out_footer boolean;
alter table t_content add column title_content varchar(2000);
alter table t_content add column description_content varchar(2000);
alter table t_content add column keywords_content varchar(2000);
alter table t_content add column cpu_content varchar(200);

create table t_realization(id_realization int primary key auto_increment, name_realization varchar(3000), 
company_realization varchar(2000), inn_realization varchar(200), address_realization varchar(2000),
phone_realization varchar(20), email_realization varchar(200), comment_realization text, created_realization timestamp default current_timestamp,
dostavka_realization varchar(100),tovar_realization varchar(3000));

alter table t_realization add column summ_realization int;













