$(document).ready(function() {
    
    $('.mob_top_menu_btn').click(function(e){
        e.preventDefault();
        $('.top_menu').slideToggle();
        $(this).toggleClass('active');
    });
    
    $('.inp_file a').click(function(e){
        e.preventDefault();
        $('.inp_file input[type="file"]').click();
    });
    
    $('.inp_file input[type="file"]').change(function(){
        console.log($(this).val());
        $('.inp_file a').text($(this).val());
    });
    
    $('.accordion>a').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).parent().find('div').stop().slideToggle();
    });
    
    
    
    $('.first_type_btn').click(function(e){
        e.preventDefault();
        $(this).addClass('active');
        $('.second_type_btn').removeClass('active');
        $('#catalog .goods_list li').removeClass('active');
    });
    $('.second_type_btn').click(function(e){
        e.preventDefault();
        $(this).addClass('active');
        $('.first_type_btn').removeClass('active');
        $('#catalog .goods_list li').addClass('active');
        $('#catalog .goods_list li div').addClass('clearfix');
    });
    
    $('.head_basket_opener_btn').click(function(e){
        e.preventDefault();
        $(this).parent('.head_basket').find('.head_basket_open').stop().slideToggle();
    });
    
    /*Tab*/
    $('#tab .tab_btn').click(function() {
        var tab_id=$(this).attr('id');
        tabClick(tab_id)
    });
    function tabClick(tab_id) {
        if (tab_id != $('#tab .tab_btn.active').attr('id') ) {
            $('#tab .tab_btn').removeClass('active');
            $('#'+tab_id).addClass('active');
            $('#tab div').removeClass('active');
            $('#con_'+ tab_id).addClass('active');
        }
    }
    /*Tab*/
    
    
    
    $('.plus').click(function(e){
        e.preventDefault();
        var $this = $(this);
        allNumberPricePlus($this);
        var $num = $this.siblings('input').val();
        $num = +$num;
        $num++;
        $this.siblings('input').val($num);
        allNumberPrice();
    });
    $('.minus').click(function(e){
        e.preventDefault();
        var $this = $(this);
        var $num = $this.siblings('input').val();
        $num = +$num;
        $num--;
        if($num < 1) {
            return false;
        }
        allNumberPriceMinus($this);
        $this.siblings('input').val($num);
        allNumberPrice();
    });
    
    allNumberPrice();
    
    function allNumberPricePlus($this) {
        var onePrice = $this.parents('.count').next('.price').find('.one_price').text();
        onePrice = onePrice.replace(/\s{1,}/g, '');
        onePrice = +onePrice;
        var $num = $this.siblings('input').val();
        $num = +$num;
        var costOnePrice = onePrice/$num;
        $this.parents('.count').next('.price').find('.one_price').text(XFormatPrice(costOnePrice*($num+1)));
    }
    
    function allNumberPriceMinus($this) {
        var onePrice = $this.parents('.count').next('.price').find('.one_price').text();
        onePrice = onePrice.replace(/\s{1,}/g, '');
        onePrice = +onePrice;
        var $num = $this.siblings('input').val();
        $num = +$num;
        var costOnePrice = onePrice/$num;
        $this.parents('.count').next('.price').find('.one_price').text(XFormatPrice(costOnePrice*($num-1)));
    }
    
    function allNumberPrice() {
        var contArray = $('.one_price'),
            allPrice = 0;
        for (var i = 0; i< contArray.length; i++) {
            var $price = $('.one_price').eq(i).text();
            $price = $price.replace(/\s{1,}/g, '');
            $price = +$price;
            console.log($price);
            allPrice += $price;
            
        }
        $('.all_price').text(XFormatPrice(allPrice));
    }
    
    $("#mail").keyup(function () {
        var email = $("#mail").val();

        if (email != 0) {
            if (isValidEmailAddress(email)) {
                $(".mail").css({
                    "border-color": "green",
                    "color": "green"
                });
                console.log("valid");
                } else {
                $(".mail").css({
                    "border-color": "#e10025",
                    "color": "#e10025"
                });
                console.log("false");
            }
        } 
        else {
            $(".mail").css({
                "border-color": "#e9e9e9"
            });
            console.log("else");
        }

    });

     function isValidEmailAddress(emailAddress) {
         var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
         return pattern.test(emailAddress);
     } 
    
    
    function XFormatPrice(_number) {
        var decimal=0;
        var separator=' ';
        var decpoint = '.';
        var format_string = '#';

        var r=parseFloat(_number)

        var exp10=Math.pow(10,decimal);// приводим к правильному множителю
        r=Math.round(r*exp10)/exp10;// округляем до необходимого числа знаков после запятой

        rr=Number(r).toFixed(decimal).toString().split('.');

        b=rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1"+separator);

        r=(rr[1]?b+ decpoint +rr[1]:b);
        return format_string.replace('#', r);
    }
});