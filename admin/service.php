<html>
    <head>
        <meta charset="utf8">
        <title>Услуги</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="tinymce/tinymce.min.js"></script>
            <script>tinymce.init({
            	language: 'ru', 
                force_p_newlines : false,
                forced_root_block : false,
                selector:'textarea',
                plugins: ['code autolink link table media jbimages'],
                theme_advanced_buttons1 : "jbimages",
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();

$t_service='t_service';
$t_user='t_user';
$conn_service=new dbquery($connect, $t_service);
?>
  <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
            <div class="col-md-10">
                <div class="table_div">
                    <table class="table">
                        <thead>
                        <th>id</th>
                        <th>Название</th>
                        <th>Текст</th>
                        <th>Изображение</th>
                        <th>Фейковая цена</th>
                        <th>Реальная цена</th>
                        <th>Пользователь</th>
                        <th>Действие</th>
                        </thead>
                        <tbody>
                        <?php 
                        $query_service=$conn_service->selectJoin($t_user, "id_user");
                        foreach ($query_service as $val){
                            echo "<tr>";
                            echo "<td>{$val['id_service']}</td>";
                            echo "<td>{$val['name_service']}</td>";
                            echo "<td>".str200($val['text_service'])."</td>";
                            echo "<td><img src='{$val['img_service']}' height=100/></td>";
                            echo "<td>{$val['fake_service']}</td>";
                            echo "<td>{$val['price_service']}</td>";
                            echo "<td>{$val['login_user']}</td>";
                            echo "<td><form name='delete' action='/admin/controller/service_controller.php' method='POST'>"
                                        . "<input name='id' value='{$val['id_service']}' style='display:none'>"
                                        . "<input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                                        . "<form name='update'>"
                                        . "<input name='id' value='{$val['id_service']}' style='display:none'>"
                                        . "<input type='submit' name='update' class='btn btn-success' value='Изменить'></form></td>";
                            echo "</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                <form method="POST" action="/admin/controller/service_controller.php" enctype="multipart/form-data">
                    <p class="lead">Добавление услуг</p>
                    <input name="id" id="id" style="display: none"/>
                    <input name="name" id="name" placeholder="Название" class="form-control"/><br/>
                    <p class="lead">Текст</p>
                    <textarea name="text" id="text" rows="15"></textarea><br/>
                    <p class="lead">Изображение</p>
                    <input type="file" accept="image/*" name="img-file" id="img-file"/>
                    <img id="img"/>
                    <br/>
                    <input type="number" id="fake" name="fake" class="form-control" placeholder="Фейковая цена"/>
                    <br/>
                    <input type="number" id="price" name="price" class="form-control" placeholder="Прайсовая цена"/>
                    <br/>
                    <input type="text" id="title" name="title" class="form-control" placeholder="Title"/>
                    <br/>
                    <input type="text" id="description" name="description" class="form-control" placeholder="description"/>
                    <br/>
                    <input type="text" id="keywords" name="keywords" class="form-control" placeholder="keywords"/>
                    <br/>
                    <input type="submit" class="form-control btn-primary" value="Сохранить">
                </form>
                </div>
            </div>
        </div>
      <script>
      $('form[name=delete]').submit(function(){
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        })
        //Update
        $('form[name=update]').submit(function(){
            var form=$(this);
            $.ajax({
                type:"post",
                url:"/admin/controller/service_controller.php",
                data: form.serialize()+"&update",
                dataType: "json",
                success: function(data){
                    res=$.parseJSON=data;
                    $("#id").val(res['id']);
                    $("#name").val(res['name']);
                    $("#text").text(res['text']);
                    $("#img").attr('src',res['img']);
                    $("#img").attr("height", 200);
                    $("#fake").val(res['fake']);
                    $("#price").val(res['price']);
                    $("#title").val(res['title']);
                    $("#description").val(res['description']);
                    $("#keywords").val(res['keywords']);
                    tinyMCE.editors[0].setContent(res['text']);
                }
            })
            return false;
        })
      </script>
  </body>
</html>