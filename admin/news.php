<html>
    <head>
        <meta charset="utf8">
        <title>Новости</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="tinymce/tinymce.min.js"></script>
            <script>tinymce.init({
            	language: 'ru', 
                force_p_newlines : false,
                forced_root_block : false,
                selector:'textarea',
                plugins: ['code autolink link table media jbimages'],
                theme_advanced_buttons1 : "jbimages",
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();

$table='t_news';
$table_content='t_content';
$conn=new dbquery($connect, $table);
?>
    <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
            <div class="col-md-10">
                <div class="table_div">
                    <table class="table">
                        <thead>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Текст</th>
                            <th>Изображение</th>
                            <th>Создано</th>
                            <th>Изменено</th>
                            <th>ЧПУ</th>
                            <th>Действие</th>
                        </thead>
                        <tbody>
                            <?php
                                $result=$conn->selectJoin($table_content, "id_content");
                                foreach($result as $row){
                                    echo '<tr>';
                                    echo "<td>{$row['id_news']}</td>";
                                    echo "<td>{$row['name_news']}</td>";
                                    echo "<td>".str200($row['text_content'])."</td>";
                                    echo "<td><img src='{$row['img_content']}' height=100></td>";
                                    echo "<td>".dateNorm($row['created_news'])."</td>";
                                    echo "<td>".dateNorm($row['updated_news'])."</td>";
                                    echo "<td>{$row['cpu']}</td>";
                                    echo "<td><form name='delete' action='/admin/controller/news-controller.php' method='POST'>"
                                        . "<input name='id' value='{$row['id_news']}' style='display:none'>"
                                        . "<input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                                        . "<form name='update'>"
                                        . "<input name='id' value='{$row['id_news']}' style='display:none'>"
                                        . "<input type='submit' name='update' class='btn btn-success' value='Изменить'></form></td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
                <p class="lead">Редактирование новостей</p>
                <div class="form-group">
                    <form method="POST" action="/admin/controller/news-controller.php" enctype="multipart/form-data">
                        <input name="id" id="id" value="" style="display:none">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Название новости"><br/>
                        <p class="lead">Текст Новости</p>
                        <textarea name="text" id="text" placeholder="Текст новости" rows="15" class="form-control"></textarea><br/>
                        <input type="file" name="file" accept="image/*"><img id="img"><br/><br/>
                        <input class="form-control" type="text" name="title" id="title" placeholder="title"><br>
                        <input class="form-control" type="text" name="descr_seo" id="descr_seo" placeholder="description"><br>
                        <input class="form-control" type="text" name="keywords" id="keywords" placeholder="keywords"><br>
                        <input type="submit" class="form-control btn-primary" value="Сохранить">
                    </form>
                </div>
            </div>
        </div>
        <script>
            $('form[name=update]').submit(function(){
            form=$(this);
            $.ajax({
                type: 'post',
                url: '/admin/controller/news-controller.php',
                dataType: 'json',
                data: form.serialize()+'&update',
                success: function(data){
                    var req=$.parseJSON=data;
                            $("#id").val(req["id"]);
                            $("#name").val(req["name"]);
                            $("#text").text(req["text"]);
                            $("#img").attr("src", req["img"]);
                            $("#img").attr("height", "200");
                            $('#title').val(req['title']);
                            $('#descr_seo').val(req['descr_seo']);
                            $('#keywords').val(req['keywords']);
                            tinyMCE.activeEditor.setContent(req['text']);
                }
            })
            return false})
            
                    $('form[name=delete]').submit(function(){
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        })
        </script>
    </body>
</html>