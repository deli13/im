<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/admin/model/dbquery.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of basket_cart
 *
 * @author deli13
 */
class basketCart extends dbquery {

    protected $cart;
    //private $dbquery;
    
    public function __construct($connect, $table, $cart){
        if ((is_array($cart)) || ($cart=="")){
            $this->cart=$cart;
            parent::__construct($connect, $table);
        } else {
            die("Неправильно переданы параметры");
        }
        
        
    }
    public function getBasket(){
        return $this->cart;
    }
    
    public function viewBasket(){ //Просмотр корзины
        $summ=0;
        $count_tov=0;
        foreach ($this->cart as $val){
            $where="id_product={$val['id']}";
            $price_cena=parent::select($where);
            $summ=$summ+$price_cena[0]['price_product']*$val['kol'];
            $count_tov++;
        }
        return array("sum"=>$summ, 'count'=>$count_tov);
    }
    
    public function addBasket($id){ //Добавление в корзину
        if (!is_numeric($id)) die("Неверный формат данных");
        $index=true;
        $where="id_product={$id}";
        $query_product=parent::select($where);
        if (count($query_product)<1){ //Если товар не найден выводится ошибка
            return "{'not':'not product'}";
        }

        $count_cart=count($this->cart);
        foreach ($this->cart as $key=>$val){
            if ($val['id']==$id){
                $this->cart[$key]['kol']++;
                $index=false;
                break;
            }
        }
        
        if ($index){ //Если не существует добавляем
            $cart_new=array("id"=>$id, "kol"=>1);
            $this->cart[]=$cart_new;
        }
        return $this->cart;
    }
    
    public function removeBasket($id){ //Удаление из корзины
        if (!is_numeric($id)) die("Неверный формат данных");
        foreach($this->cart as $key=>$val){
            if ($val['id']==$id) unset($this->cart[$key]);
        }
    }
    
    public function updateBasket($array){
        $cart=array();
        foreach ($array as $key =>$val){
            $cart[]=array("id"=>$key, "kol"=>$val);
        }
        $this->cart=$cart;
    }
    
}
