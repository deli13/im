<?php

function valid_input($znach){ //проверка введёных значений
    if (!is_numeric($znach)){
    
        $result = htmlentities(stripcslashes($znach));
    
    } else $result=$znach;
    return $result;
}

function session(){ //Старт сессии
    if (!isset($_SESSION)){
        session_start();
		ini_set('session.gc_maxlifetime', 1800);
		ini_set('session.cookie_lifetime', 1800);
    }
}

function translit($url){ //Создание транслитерации для ЧПУ
    $translit_table= array(
        "а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=> "_", "."=> "", "/"=> "_", '"'=>"", "'"=>"", "№"=>"num"
    );
    $url=  trim(mb_strtolower($url, 'UTF-8'));
            return strtr($url, $translit_table);
}

function crypto($pass){  //Хэш пароля
    $salt="239366";
    return sha1(md5($pass).$salt);
}

function dateNorm($date){  //Преобразование даты в человечий вид
    return date("d.m.Y G.i.s", strtotime($date));
}

function translit1($url){ //Создание транслитерации для image
    $translit_table= array(
        "а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=> "_", "."=> ".", "/"=> "_", '"'=>"", "'"=>"", "№"=>"num"
    );
    $url=  trim(mb_strtolower($url, 'UTF-8'));
            return strtr($url, $translit_table);
}

function imageUpload($file){
    $uploaddir='../../images/';
    $tmp=md5(microtime(true))."_".basename($file["name"]);
    $tmp=translit1($tmp);
    $uploadfile=$uploaddir.$tmp;
    if (move_uploaded_file($file['tmp_name'], $uploadfile)){
        return '/images/'.$tmp;
    } else {
        echo $file['error']."<br>";
        return false;
    }
}

function fileUpload($file){
    $uploaddir=$_SERVER['DOCUMENT_ROOT']."/source/";
    $tmp= translit1(basename($file['name']));
    $uploadfile=$uploaddir.$tmp;
    if (move_uploaded_file($file['tmp_name'], $uploadfile)){
        return $uploadfile;
    } else {
        echo $file['error']."<br>";
        return false;
    }
}

function str200($text){
    $text_pr=  strip_tags($text); 
    if (strlen($text)>200){
        $text_pr=mb_substr($text_pr, 0, 200, 'UTF-8');
    }
    return $text_pr;
}

function strleng($text, $length){
    $text_pr=strip_tags($text);
    if (strlen($text_pr)>$length){
        $text_pr=mb_substr($text_pr,0,$length, 'UTF-8');
        }
    return $text_pr;
}

function postToUrl($url, $param){
	$result=file_get_contents($url, false, stream_context_create(array(
	'http'=>array(
	'method'=>'POST',
	'header'=>'Content-type: application/x-www-form-urlencoded',
	'content'=>http_build_query($param)
		)
	)));
	return $result;
}

function mailSend($mail, $mailclient, $subject, $msg){
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	$to=$mailclient.", ".$mail;
	
	// Дополнительные заголовки
	$headers .= "To: klient <{$mailclient}>, w2you <{$mail}>" . "\r\n";
	$headers .= "From:  w2you <{$mail}>" . "\r\n";
	if(mail($to, $subject, $msg, $headers)){
		return true;
	} else {
		return false;
	}
	
}

function fileArray($file){
    $file_ary=array();
    $count=count($file['name']);
    $file_key= array_keys($file);
    for ($i=0;$i<$count; $i++){
        foreach($file_key as $key){
            $file_ary[$i][$key]=$file[$key][$i];
        }
    }
    return $file_ary;
}

if(!function_exists('mb_ucfirst')) {
    function mb_ucfirst($str, $enc = 'utf-8') { 
    		return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc); 
    }
}


function number2string($number) { //Спизжено с хабра
	
	// обозначаем словарь в виде статической переменной функции, чтобы 
	// при повторном использовании функции его не определять заново
	static $dic = array(
	
		// словарь необходимых чисел
		array(
			-2	=> 'две',
			-1	=> 'одна',
			1	=> 'один',
			2	=> 'два',
			3	=> 'три',
			4	=> 'четыре',
			5	=> 'пять',
			6	=> 'шесть',
			7	=> 'семь',
			8	=> 'восемь',
			9	=> 'девять',
			10	=> 'десять',
			11	=> 'одиннадцать',
			12	=> 'двенадцать',
			13	=> 'тринадцать',
			14	=> 'четырнадцать' ,
			15	=> 'пятнадцать',
			16	=> 'шестнадцать',
			17	=> 'семнадцать',
			18	=> 'восемнадцать',
			19	=> 'девятнадцать',
			20	=> 'двадцать',
			30	=> 'тридцать',
			40	=> 'сорок',
			50	=> 'пятьдесят',
			60	=> 'шестьдесят',
			70	=> 'семьдесят',
			80	=> 'восемьдесят',
			90	=> 'девяносто',
			100	=> 'сто',
			200	=> 'двести',
			300	=> 'триста',
			400	=> 'четыреста',
			500	=> 'пятьсот',
			600	=> 'шестьсот',
			700	=> 'семьсот',
			800	=> 'восемьсот',
			900	=> 'девятьсот'
		),
		
		// словарь порядков со склонениями для плюрализации
		array(
			array('рубль', 'рубля', 'рублей'),
			array('тысяча', 'тысячи', 'тысяч'),
			array('миллион', 'миллиона', 'миллионов'),
			array('миллиард', 'миллиарда', 'миллиардов'),
			array('триллион', 'триллиона', 'триллионов'),
			array('квадриллион', 'квадриллиона', 'квадриллионов'),
			// квинтиллион, секстиллион и т.д.
		),
		
		// карта плюрализации
		array(
			2, 0, 1, 1, 1, 2
		)
	);
	
	// обозначаем переменную в которую будем писать сгенерированный текст
	$string = array();
	
	// дополняем число нулями слева до количества цифр кратного трем,
	// например 1234, преобразуется в 001234
	$number = str_pad($number, ceil(strlen($number)/3)*3, 0, STR_PAD_LEFT);
	
	// разбиваем число на части из 3 цифр (порядки) и инвертируем порядок частей,
	// т.к. мы не знаем максимальный порядок числа и будем бежать снизу
	// единицы, тысячи, миллионы и т.д.
	$parts = array_reverse(str_split($number,3));
	
	// бежим по каждой части
	foreach($parts as $i=>$part) {
		
		// если часть не равна нулю, нам надо преобразовать ее в текст
		if($part>0) {
			
			// обозначаем переменную в которую будем писать составные числа для текущей части
			$digits = array();
			
			// если число треххзначное, запоминаем количество сотен
			if($part>99) {
				$digits[] = floor($part/100)*100;
			}
			
			// если последние 2 цифры не равны нулю, продолжаем искать составные числа
			// (данный блок прокомментирую при необходимости)
			if($mod1=$part%100) {
				$mod2 = $part%10;
				$flag = $i==1 && $mod1!=11 && $mod1!=12 && $mod2<3 ? -1 : 1;
				if($mod1<20 || !$mod2) {
					$digits[] = $flag*$mod1;
				} else {
					$digits[] = floor($mod1/10)*10;
					$digits[] = $flag*$mod2;
				}
			}
			
			// берем последнее составное число, для плюрализации
			$last = abs(end($digits));
			
			// преобразуем все составные числа в слова
			foreach($digits as $j=>$digit) {
				$digits[$j] = $dic[0][$digit];
			}
			
			// добавляем обозначение порядка или валюту
			$digits[] = $dic[1][$i][(($last%=100)>4 && $last<20) ? 2 : $dic[2][min($last%10,5)]];
			
			// объединяем составные числа в единый текст и добавляем в переменную, которую вернет функция
			array_unshift($string, join(' ', $digits));
		}
	}
	
	// преобразуем переменную в текст и возвращаем из функции, ура!
	return join(' ', $string);
}



?>

