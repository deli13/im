<html>
    <head>
        <meta charset="utf8">
        <title>Файловый менеджер</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
            <script>tinymce.init({ 
                force_p_newlines : false,
                forced_root_block : false,
                selector:'textarea',
                plugins: ['code autolink link image table']
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
session();
$index="Location: /admin/index.php";
if ($_SESSION["role"]!="admin"){
    header($index);
}
?>
    <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
            <div class="col-md-10">
            <?php include("filemanager.php"); ?>
            </div>
        </div>
    </body>
</html>