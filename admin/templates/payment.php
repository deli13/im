<?php
include_once 'config.php';
include_once $root.'/admin/asset/basket_cart.class.php';

session();
if (!isset($_SESSION['cart'])){
    $_SESSION['cart']=array();
}
$cart=$_SESSION['cart'];
$tproduct="t_product";
$Basket=new basketCart($connect, $tproduct, $cart);
$id=0;
$add=0;



if ((isset($_POST['addToCart'])) && (is_numeric($_POST['id']))){ //Добавление товара в корзину
    $id=$_POST['id'];
    $cart=$Basket->addBasket($id);
    $_SESSION['cart']=$cart;    
    $arr_json= $Basket->viewBasket();
    echo json_encode($arr_json);

}

if (isset($_POST['view_cart'])){
    $json_arr= $Basket->viewBasket();
    echo json_encode($json_arr);
}

//if ((isset($_GET['remove'])) && (is_numeric($_GET['remove']))){
//    $id=$_GET['remove'];
//    $Basket->removeBasket($id);
//    $_SESSION['cart']=$Basket->getBasket();
//    header("Location: {$referer}");
//    
//}

if ((isset($_POST['update'])) && ($_POST['data']!="")){
    $jsonarr= json_decode($_POST['data'], 1);
    if (is_array($jsonarr)){
        $Basket->updateBasket($jsonarr);
        $_SESSION['cart']=$Basket->getBasket();
        echo '{"yes":"1"}';
    }
    
}


if (isset($_POST['fio']) && $_POST['fio']!=""){
    require_once $root.'/admin/asset/vendor/autoload.php';
    $oplata= valid_input($_POST['type_price']);
    $fio= valid_input($_POST['fio']);
    $dostavka=$_POST['drive'];
    $company= valid_input($_POST['company']);
    $inn= valid_input($_POST['inn']);
    $addr= valid_input($_POST['address']);
    $phone= valid_input($_POST['phone']);
    $email= filter_input(INPUT_POST,'email',FILTER_SANITIZE_EMAIL);
    $comm= valid_input($_POST['comment']);
    $ur_addr= valid_input($_POST['ur-address']);
    $file=($_FILES['recvesit']['name']!="")?fileUpload($_FILES['recvesit']):false;
    if ($file){
        $name_file= array_pop(explode("/", $file));
    }
    $basket=$Basket->getBasket();
    $summ=$Basket->viewBasket();
    $realization='t_realization';
    $conn_realization=new dbquery($connect, $realization);
    
    $field=array('name_realization', 'company_realization', 'inn_realization', 'address_realization',
     'phone_realization',  'email_realization', 'comment_realization', 'dostavka_realization', 'tovar_realization',
      'summ_realization');
    $value=array($fio,$company,$inn,$addr,$phone,$email,$comm,$dostavka,json_encode($basket),$summ['sum']);
    $id_realization=$conn_realization->insert($field, $value);
    $_SESSION['id_real']=$id_realization;
    
    $text="Письмо с сайта Акбор honda сервис<br/>"
            . "ФИО: {$fio}<br/>"
            . "Способ доставки {$dostavka}<br/>"
            . "Способ оплаты {$oplata}<br/>"
            . "{$company}<br/>"
            . "{$inn}<br/>"
            . "Адрес: {$addr}<br/>"
            . "Юридический адрес: {$ur_addr}<br/>"
            . "Телефон: {$phone}<br/>"
            . "Email: {$email}<br/>"
            . "Комментарий к заказу: {$comm} <br/> Ваш заказ:";
            $text.="<table>";
            $text.="<thead><th>Название</th><th>Количество</th><th>Цена за штуку</th></thead>";
     foreach($basket as $val){
         $text.="<tr>";
         $query=$Basket->select("id_product={$val['id']}");
         $text.="<td>".$query[0]['name_product']."</td>";
         $text.="<td>".$val['kol']."</td>";
         $text.="<td>".$query[0]['price_product']."</td>";
         $text.="</tr>";
     }   
     $text.="</table>";
     
     $text.="Итого: {$summ['sum']}";
     //echo $text;
    

    $mail=new PHPMailer;
    //$mail->isSMTP();
    $mail->CharSet="UTF-8";
    $mail->setFrom(mail_akbor);
    $mail->addAddress(mail_akbor);
    $mail->addAddress($email);
    if ($file) $mail->addAttachment($file, $name_file);
    $mail->isHTML(true);
    $mail->Subject="Письмо с сайта Акбор honda сервис";
    $mail->Body=$text;
    if($mail->send()){
        unlink($file);
        header("Location: http://{$host}{$append_uri}");
    }//Ты Справишься!!!!!
    else {
        echo 'Письмо не отправлено '.$mail->ErrorInfo.' <br/><a href="http://'.$host.'">Вернуться на главную страницу</a>';
    }
}