<?php include_once 'config.php';
$t_content='t_content';
$conn_text=new dbquery($connect, $t_content);
$cpu=$connect->quote($cpu);
$where="cpu_content={$cpu}";
$query_text=$conn_text->select($where);
if (count($query_text)<1) header("Location: /404");
$content=$query_text[0];
$head_content=$content['name_content'];
$text_content=$content['text_content'];
$img_content=$content['img_content'];
$title=$content['title_content'];
$description=$content['description_content'];
$keywords=$content['keywords_content'];

?>
<!DOCTYPE html>
<html lang="en">
<?php include 'head.php'?>
<body>
<?php include 'header.php'?>
<div id="text_page">
    <div class="content">
        <div class="text_page">
            <div class="breadcrumbs">
                <ul class='clearfix'>
                    <li><a href="/">Главная</a></li>
                    <li><span><?php echo $head_content?></span></li>
                </ul>
            </div>
            <h1><?php echo $head_content?></h1>
            <div class="text_main_block clearfix">
                <p>
                  <?php
				  if ($img_content<>'/images/noimage.png')
				  echo '<img src="'.$img_content.'" alt="" style="max-height: 400px">';
				  echo $text_content;
				  ?>
                    </p>
            </div>

        </div>
    </div>
</div>
<?php include 'footer.php'?>
<?php include 'script.php'?>
</body>
</html>