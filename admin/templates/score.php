<?php include "config.php";
define("nds", 1.18);
$conn_realization=new dbquery($connect, 't_realization');
$conn_product=new dbquery($connect, 't_product');


if ((isset($_GET['id_score'])) && (is_numeric($_GET['id_score']))){
    $id= valid_input($_GET['id_score']);
    $where="id_realization={$id}";
    $query_realization=$conn_realization->select($where);
    if (count($query_realization)!=1){
        header ("Location: /");
    }
    $name=$query_realization[0]["name_realization"];
    $company=$query_realization[0]["company_realization"];
    $created=dateNorm($query_realization[0]['created_realization']);
    $summ=$query_realization[0]["summ_realization"];
    $summ_text= number2string($summ);
    $tovar=json_decode($query_realization[0]["tovar_realization"], 1);
    $count_tovar=count($tovar);
    $i=1;
    $count_summ=0;
} else {
    header("Location: /");
}
if (date("d.m.Y") != $created){
    header("Location: /");
}

?>
<html>
    <head>
    <meta charset="utf-8">
        <style>
            div{
                display: block;
                margin:0px;
                float: left;
            }
            div.row{
                position: relative;
                width:100%;
            }
            div.row1{
                width:70%;
            }
            div.row2{
                width:30%;
            }
            div.center{
                padding-top: 15px;
                margin-bottom: 10px; 
                margin-left: 35%;
                margin-right: 35%;
                text-align: center;
            }
            h3{
                text-decoration: underline;
            }
            h2{
                text-align: center;
            }
            table{
                width:95%;
                margin: 2% 0 2% 0;
                border-collapse: collapse;
            }
            table td{
                border: 1px solid black;
            }
            div.rightcol{
                float: none;
                margin-left: 70%
            }
        </style>
        <script>
        document.addEventListener("DOMContentLoaded", function(){
            window.print()
        })
        </script>
    </head>
    <body>
        <h3>Договор поставки товаров, услуг</h3>
        <div class="row">
            <div class="row1">
                <span><?php echo date("d.m.Y")?>г.</span><br/><br/>
                <span>ООО "АКБОР", в лице Генерального директора Плясунова В.Н. именуемое в дальнейшем "Поставщик", </span><br/>
                <span>и <?php echo $company?>, в лице представителя <?php echo $name ?> </span><br/>
                <span>именуемое в дальнейшем "Покупатель"  заключили договор о нижеследующем :</span><br/>
                <ol>
                    <li>Поставщик продает, а Покупатель покупает товар, услуги в количестве и по ценам, указанным в счете №  <?php echo $id?> , являющимся  неотъемлемой частью настоящего договора.</li>
                    <li>Товар имеет необходимые сертификаты</li>
                    <li>Условия оплаты: предоплата 100% в течении 5 банковских дней</li>
                    <li>Оплата данного счета означает подписание настоящего договора второй стороной и является действительным.</li>
                    <li>Договор составлен в двух экземплярах, имеющих равную силу.</li>
                </ol>
                <span>Внимание! Оплата данного счета означает, согласие с условиями поставки товара и выполнения услуг. Товар отпускается по факту прихода денег на р\с Поставщика, самовывозом, при наличии доверенности и паспорта. Оплата за услуги означает согласие Покупателя со стоимостью услуг и сроками их выполнения.</span>
                <br/>
                <h3>ООО "АКБОР"</h3>
                <br/>
                <strong>Адрес: 194362, Санкт-Петербург г, Парголово п, Донецкая (Торфяное) ул, д. 2 ,литера А, пом. 12, тел.: 449-04-40</strong>
                <br/>
            </div>
            <div class="row2">
                <span>г.Санкт-Петербург</span>
            </div>
        </div>
        
        <div class="center"><strong>Образец заполнения платежного поручения</strong></div>
        <table>
            <tr>
                <td>ИНН 7802607096</td>
                <td width="40"></td>
                <td>КПП 780201001</td>
                <td width="40"></td>
                <td rowspan="3">Сч. №</td>
                <td rowspan="3">40702810055080006741</td>
            </tr>
            <tr>
                <td>Получатель</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>ООО "АКБОР"</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Банк получателя</td>
                <td></td>
                <td></td>
                <td></td>
                <td>БИК</td>
                <td>044030653</td>
            </tr>
            <tr>
                <td>Северо-Западный Банк ПАО Сбербанк г. Санкт-Петербург</td>
                <td></td>
                <td></td>
                <td></td>
                <td>Сч. №</td>
                <td>30101810500000000653</td>
            </tr>
        </table>
        <h2>СЧЕТ № <?php echo $id." от ".$created?> г.</h2>
        <span>Плательщик: <?php echo $company ?> </span><br/>
        <span>Грузополучатель: <?php echo $company?></span>
        <table>
            <tr>
                <td>№</td>
                <td>Наименование товара</td>
                <td>Единица измерения</td>
                <td>Количество</td>
                <td>Цена</td>
                <td>Сумма</td>
            </tr>
            <?php foreach($tovar as $key=>$val): 
            $tovar_one=$conn_product->select("id_product={$val['id']}");
            $price_one_without_nds=$tovar_one[0]['price_product']/nds;
            $price_summ_without_nds=$price_one_without_nds*$val['kol'];
            $count_summ=$count_summ+$price_summ_without_nds;
            ?>
            <tr>
                <td><?php echo $i;?></td>
                <td><?php echo $tovar_one[0]['name_product']?></td>
                <td>шт</td>
                <td><?php echo $val['kol']?></td>
                <td><?php echo round($price_one_without_nds,2) ?></td>
                <td><?php echo round($price_summ_without_nds,2) ?></td>
            </tr>
            <?php $i++; endforeach;?>
        </table>
            <div class="rightcol">
                <?php $nds_itog=$summ-round($count_summ, 2)?>
                <strong>Итого: <?php echo round($count_summ,2)?></strong><br/>
                <strong>Итого НДС: <?php echo $nds_itog?></strong><br/>
                <strong>Всего к оплате <?php echo $summ?>,00</strong><br/>
            </div>
        <span>Всего наименований <?php echo $count_tovar; ?>, на сумму</span><br/>
        <strong><?php echo mb_ucfirst($summ_text)?> рублей 00 копеек</strong><br/>
        <br/>
        <span>Руководитель предприятия_____________________ (Плясунов В.Н.)</span>
        <br/>
        <span>Главный бухгалтер____________________________ (Рубцова И.А.)</span>
    </body>
</html>