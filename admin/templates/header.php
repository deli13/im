<?php 
$t_catalog_header="t_catalog";
$conn_header=new dbquery($connect, $t_catalog_header);
$query_header=$conn_header->select("parent_catalog is null");
unset($conn_header);
$t_content_header='t_content';
$conn_content_header=new dbquery($connect, $t_content_header);
$query_cont_header=$conn_content_header->select("out_header=1 LIMIT 5");
unset($conn_content_header);
?>
<div id="top">
    <div class="content">
        <div class="top clearfix">
                        <ul class="top_menu clearfix">
                <?php foreach($query_cont_header as $val): ?>
                <li>
                    <a href="<?php echo $text_uri.'/'.$val['cpu_content']?>"><?php echo $val['name_content']?></a>
                </li>
                <?php endforeach;?>
                <li>
                  <a href="<?php echo $call_uri ?>" style="font-weight:800">Запись на ремонт и ТО</a>
                </li>

            </ul>
            <div class="mob_top_menu">
                <a href="#" class="mob_top_menu_btn">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>
            <div class="tel">
                <a href="tel:8 812 449 03 30">8 812 449 04 40</a><br/>
                <a href="tel:8 812 449 04 40">8 931 239 55 72</a>
            </div>
			
            <ul class="mob_head_menu clearfix">
                <?php foreach ($query_header as $val): ?>
                <li>
                    <a href="<?php echo $catalog_uri."/".$val['cpu_catalog']?>"><?php echo $val['name_catalog']?></a>
                </li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>
<div id="header">
    <div class="content">
        <div class="header clearfix">
            <div class="logo">
                <a href="#">
                    <img src="img/logo.png" alt="">
                </a>
            </div>
            <ul class="head_menu clearfix">
                <?php foreach ($query_header as $val): ?>
                <li>
                    <a href="<?php echo $catalog_uri."/".$val['cpu_catalog']?>"><?php echo $val['name_catalog']?></a>
                </li>
                <?php endforeach;?>
            </ul>
            <div class="head_basket clearfix">
                <a href="<?php echo $basket_uri?>">
                    <div class="basket_count">
                        <span id="kol_price">0</span>
                    </div>
                    <div class="basket_price">
                        <p>Сумма</p>
                        <span id="summ_price">0 руб.</span>
                    </div>
                </a>
            </div>


        </div>
    </div>
</div>