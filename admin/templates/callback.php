<?php
include_once "config.php";
?>
<!DOCTYPE html>
<html lang="en">
<?php    include 'head_zak.php';?>
    <style>
 @media (max-width: 767px){
    #your_order .your_order .your_order_right {
    display: block;
    float: none !important;
    width: 100% !important;
}}
    </style>
<body>
<?php include 'header_zak.php'?>
<div id="your_order">
    <div class="content">
        <div class="your_order clearfix">
            <div class="your_order_right">
                <h3>Записаться на ремонт</h3>

                <form  id="form" method="POST" enctype="multipart/form-data">
                    <input type="text" name="fio" required placeholder="ФИО">
                    <input type="text" name="phone" required placeholder="Телефон" name="phone">
                    <input type="text" name="email"  placeholder="E-mail" class="mail" id="mail">
                    <input type="text" name="mark"  placeholder="Марка">
                    <input type="text" name="model" placeholder="Модель">
                    <input type="number" name="age" placeholder="Год выпуска">
                    <input type="number" name="probeg" placeholder="Пробег">
                    <p>Желаемая дата и время</p>
                    <input type="datetime-local" id="date" name="datetime">
                    <textarea name="comment" id="" cols="30" rows="10" placeholder="Неисправность"></textarea>
                    <div class="form_bottom clearfix">

                        <input type="submit" value="Оформить заявку">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include 'footer_zak.php'?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/script.js"></script>
<script>
    /*Phone input placehoder mask*/
    jQuery(function($){
       $('input[name="phone"]').mask("+7 (999) 999-99-99");
    });
    
    $("form").submit(function(){
        var forms=$(this)
        $.ajax({
            method: "post",
            url:"<?php echo $send_uri; ?>",
            data:forms.serialize(),
            dataType:"html",
            success:function(data){
                    var n = noty({
                        text        : "Заявка отправлена",
                        type        : "warning",
                        dismissQueue: true,
                        timeout     : 1000,
                        //closeWith   : ['click'],
                        layout      : 'bottomLeft',
                        theme       : 'relax',
                        maxVisible  : 10
                    });
            }
        })
        return false;
    })
</script>
  <script async src="http://cdn.jsdelivr.net/jquery.mcustomscrollbar/3.0.6/jquery.mCustomScrollbar.concat.min.js"></script>
</body>
</html>
