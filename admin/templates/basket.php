<?php 
include_once 'config.php';

$cart=$_SESSION['cart'];
$t_product="t_product";
$t_catalog="t_catalog";
$t_img="t_img_product";
$add_cart=array();
$conn_img=new dbquery($connect, $t_img);
$conn_product=new dbquery($connect, $t_product);
foreach ($cart as $val){
    $query_product=$conn_product->selectJoin($t_catalog, "id_catalog", "id_product={$val['id']}");
    $add_cart[]=$query_product[0];
}
$title="";
$description="";
$keywords="";
if (count($cart)==0) header("Location: http://{$host}");
//print_r($add_cart);
?>
<!DOCTYPE html>
<html lang="en">
<?php include 'head_zak.php';?>
<body>
<?php    include 'header_zak.php';?>
<div id="basket">
    <div class="content">
        <div class="basket">
            <h2>Корзина</h2>
            <div class="basket_items">
              <div class="basket_item clearfix">
                    <div class="img">
                        <div style="display: table-cell;vertical-align: middle;height: 100px;">
                            <strong>Изображение</strong>
                        </div>
                    </div>
                    <div class="name">
                        <div>
                            <strong>Наименование</strong>
                        </div>
                    </div>
                    <div class="count clearfix">
                        <div>
                            <strong>Количество</strong>
                        </div>
                    </div>
                    <div class="price">
                        <div style="display: table-cell;vertical-align: middle;height: 100px;">
                        <strong>Цена</strong>
                        </div>
                    </div>
                    <div class="delete">
                        <div style="display: table-cell;vertical-align: middle;height: 100px;">
                            <strong>Удалить</strong>
                        </div>
                    </div>
                </div>  
                <?php foreach ($add_cart as $val): 
                   $query_img=$conn_img->select("id_product={$val['id_product']}");
                   $img="";
                   foreach ($query_img as $value){
                       if (trim($value["img_product"])!=""){
                           $img=$value["img_product"];
                           break;
                       }
                   }
                   foreach($cart as $value){
                       if ($value['id']==$val['id_product']){
                           $kol=$value['kol'];
                           $summ=$val['price_product']*$kol;
                       }
                   }
                   ?> 
                <div class="basket_item clearfix" name="<?php echo $val['id_product']?>">
                    <div class="img">
                        <img src="<?php echo $img ?>" alt="">
                    </div>
                    <div class="name">
                        <div>
                            <p><?php echo $val['name_catalog']?></p>
                            <span><?php echo $val['name_product'] ?></span>
                            <input name="id" style="display: none" value="<?php echo $val['id_product']?>">
                        </div>
                    </div>
                    <div class="count clearfix">
                        <div>
                            <a href="#" class="minus">
                                <img src="img/minus.png" alt="">
                            </a>
                            <input type="text" name="kol" value="<?php echo $kol;?>">
                            <a href="#" class="plus">
                                <img src="img/plus.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="price">
                        <span><span class="one_price"><?php echo $summ;?></span> <i>₽</i></span>
                    </div>
                    <div class="delete">
                        <!--<a href="<?php echo $payment_uri."?remove=".$val['id_product']; ?>" name="delete"><img src="img/delete.png" alt=""></a>-->
                        <a href="#<?php echo $val['id_product']?>" name="delete" ><img src="img/delete.png" alt=""></a>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
            <div class="basket_total_price clearfix">
                <p>Итоговая стоимость без учета доставки</p>
                <span><span class="all_price"></span> <i>₽</i></span>
            </div>
            <div class="basket_checkout clearfix">
                <a href="/" class="back_to_btn">Вернуться к покупкам</a>
                <a href="<?php echo $zakaz_uri?>" class="checkout_btn" id="next">Оформить заказ</a>
            </div>
        </div>
    </div>
</div>
<?php include 'footer_zak.php';?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/script.js"></script>
<script>
var next_step=document.getElementById("next");
var ids_product=document.getElementsByName("id");
var kol_product=document.getElementsByName("kol");

    function XFormatPrice(_number) {
        var decimal=0;
        var separator=' ';
        var decpoint = '.';
        var format_string = '#';

        var r=parseFloat(_number)

        var exp10=Math.pow(10,decimal);// приводим к правильному множителю
        r=Math.round(r*exp10)/exp10;// округляем до необходимого числа знаков после запятой

        rr=Number(r).toFixed(decimal).toString().split('.');

        b=rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1"+separator);

        r=(rr[1]?b+ decpoint +rr[1]:b);
        return format_string.replace('#', r);
    }

    function allNumberPrice() {
        var contArray = $('.one_price'),
            allPrice = 0;
        for (var i = 0; i< contArray.length; i++) {
            var $price = $('.one_price').eq(i).text();
            $price = $price.replace(/\s{1,}/g, '');
            $price = +$price;
            console.log($price);
            allPrice += $price;
            
        }
        $('.all_price').text(XFormatPrice(allPrice));
    }

function generateIdKol(ids, kol){
    var serialize={};
    for(var i=0; i<ids.length; i++){
        serialize[ids[i].value]=kol[i].value;
    }
    return JSON.stringify(serialize);
}

next_step.addEventListener("click", function(e){
    var evt = e ? e : window.event;
    (evt.preventDefault) ? evt.preventDefault() : evt.returnValue = false;
    var jsonstr=generateIdKol(ids_product, kol_product);
    $.ajax({
        type:"post",
        url: "/payment",
        data: "data="+jsonstr+"&update",
        dataType:"json",
        success:function(data){
        var req=$.parseJSON=data;
        if (req['yes']==1){
            a=window.location.assign('<?php echo $zakaz_uri?>');
        }
        }
    })
    return false
})

$("[name=delete]").on("click", function(e){
    var name_this=$(this).attr("href");
    var evt = e ? e : window.event;
    (evt.preventDefault) ? evt.preventDefault() : evt.returnValue = false;
    var remove_node_name=name_this.substring(1);
    $("[name="+remove_node_name+"]").remove();
    allNumberPrice();
})

</script>

</body>
</html>