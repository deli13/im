<?php include_once 'config.php';
$tproduct='t_product';
$timg='t_img_product';
$tcatalog='t_catalog';

$conn_prod=new dbquery($connect, $tproduct);
$conn_img=new dbquery($connect, $timg);

$cart=$_SESSION['cart'];
$product_cart=array();
foreach($cart as $val){
    $where="id_product={$val['id']}";
    $query_prod=$conn_prod->selectJoin($tcatalog, 'id_catalog',$where);
    $product_cart[]=$query_prod[0];
}
$title="";
$description="";
$keywords="";
if (count($cart)==0) header("Location: http://{$host}");
?>
<!DOCTYPE html>
<html lang="en">
<?php    include 'head_zak.php';?>
<body>
<?php include 'header_zak.php'?>
<div id="your_order">
    <div class="content">
        <div class="your_order clearfix">
            <div class="your_order_left">
                <h3>Ваш заказ</h3>
              <div class="item_scroll_type">
                <ul>
                    <?php 
                    $summ_result=0;
                    foreach($product_cart as $val):
                        
                   $query_img=$conn_img->select("id_product={$val['id_product']}");
                   $img="";
                   foreach ($query_img as $value){
                       if (trim($value["img_product"])!=""){
                           $img=$value["img_product"];
                           break;
                       }
                   }
                   foreach($cart as $value){
                       if ($value['id']==$val['id_product']){
                           $kol=$value['kol'];
                           $summ=$val['price_product']*$kol;
                       }
                   } 
                   $summ_result=$summ_result+$summ;
                     ?>
                    <li>
                        <div class="your_order_items clearfix">
                            <div class="your_order_item_img">
                                <img src="<?php echo $img?>" alt="">
                            </div>
                            <div class="your_order_item_info">
                                <p><?php echo $val['name_catalog']?></p>
                                <span class="name"><?php echo $val['name_product']?></span>
                                <span class="kol">Количество <?php echo $kol;?></span><br/>
                                <span class="price"><?php echo $summ?><i>₽</i></span>
                            </div>
                        </div>
                    </li>
                   <?php endforeach;?>
                    <li>
                        <div class="your_order_total clearfix">
                            <div class="your_order_total_left">
                                <p>Итог</p>
                                <span><?php echo $summ_result;?><i>₽</i></span>
                            </div>
                            <div class="your_order_total_right">
                                <a href="<?php echo $basket_uri?>">Изменить заказ</a>
                            </div>
                        </div>
                    </li>
                </ul>
              </div>
            </div>
            <div class="your_order_right">
                <h3>Оформление заказа</h3>
                <ul class="your_order_tab clearfix">
                    <li>
                        <a href="#" id="yr" class="entity_btn active">Юридическое лицо</a>
                    </li>
                    <li>
                        <a href="#" id="fiz" class="individual_btn">Физическое лицо</a>
                    </li>
                </ul>
                <form action="<?php echo $payment_uri?>" method="POST" enctype="multipart/form-data">
                    <select name="type_price">
                        <option disabled selected>Способ оплаты</option>
                        <option value="в кассу продавца">в кассу продавца</option>
                        <option value="на банковскую карту продавца">на банковскую карту продавца</option>
                        <option value="на р/с продавца через сбербанк">на р/с продавца через сбербанк</option>
                        <option value="с карты visa/master cart/ maestro / Мир">с карты visa/master cart/ maestro / Мир</option>
                    </select>
                    <select name="drive" id="">
                        <option disabled selected>Способ доставки</option>
                        <option value="Самовывоз">Самовывоз</option>
                        <option value="Доставка транспортной компанией">Доставка транспортной компанией</option>
                        <option value="Доставка продавца">Доставка продавца</option>
                    </select>
                    <input type="text" name="fio" placeholder="ФИО контактного лица">
                    <input type="text" id="comp" name="company" placeholder="Наименование компании">
                    <input type="text" id="inn" name="inn" placeholder="ИНН">
                    <input type="text" name="address" placeholder="Адрес доставки">
                    <input type="text" name="ur-address" id="ur-add" placeholder="Юридический адрес">
                    <input type="text" name="phone" placeholder="Телефон" name="phone">
                    <input type="text" name="email" placeholder="E-mail" class="mail" id="mail">
                    <textarea name="comment" id="" cols="30" rows="10" placeholder="Комментарий к заказу"></textarea>
                    <p>Прислать доп. реквезиты можно по эл. почте <a href="mailto: info@akbor.spb.ru">info@akbor.spb.ru</a></p>
                    <div class="form_bottom clearfix">
                        <div class="inp_file">
                            <a href="#">Прикрепить реквезиты</a>
                            <input name="recvesit" type="file">
                            <h6>Размер не должен превышать 2 мб</h6>
                        </div>
                        <input type="submit" value="Оформить заказ">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include 'footer_zak.php'?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/script.js"></script>
<script>
    /*Phone input placehoder mask*/
    jQuery(function($){
       $('input[name="phone"]').mask("+7 (999) 999-99-99");
    });
    
    var yr=document.getElementById("yr");
    var phiz=document.getElementById("fiz");
    
    phiz.addEventListener("click", function(e){
        var evt = e ? e : window.event;
        (evt.preventDefault) ? evt.preventDefault() : evt.returnValue = false;
        $("#yr").removeClass("active");
        $("#fiz").addClass("active");
        $("#comp").css("display", "none");
        $("#inn").css("display", "none");
        $("#ur-add").css("display", "none");
    });
    yr.addEventListener("click", function(e){
        var evt = e ? e : window.event;
        (evt.preventDefault) ? evt.preventDefault() : evt.returnValue = false;
        $("#yr").addClass("active");
        $("#fiz").removeClass("active");
        $("#comp").css("display", "inline-block");
        $("#inn").css("display", "inline-block");
        $("#ur-add").css("display", "inline-block");
    })
</script>
  <script async src="http://cdn.jsdelivr.net/jquery.mcustomscrollbar/3.0.6/jquery.mCustomScrollbar.concat.min.js"></script>
</body>
</html>