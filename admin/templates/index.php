<?php include_once 'config.php';
$t_catalog="t_catalog";
$t_product="t_product";
$t_char_prod="t_character_product";
$t_img="t_img_product";
$t_char="t_сharaсter";
$conn_catalog=new dbquery($connect, $t_catalog);
$conn_product=new dbquery($connect, $t_product);
$conn_char_prod=new dbquery($connect, $t_char_prod);
$conn_img=new dbquery($connect, $t_img);
$conn_char=new dbquery($connect, $t_char);
$query_product=$conn_product->select("viv_main=1");
?>
<!DOCTYPE html>
<html lang="en">
<?php include "head.php"?>
<body>
<?php include 'header.php' ?>
<div id="main_slider">
    <div id="owl_main_slider" class="owl-carousel">
        <div class="item">
            <img src="img/main_bg.jpg" alt="" class="backfround">
            <div class="content">
                <div class="item_block">
                    <h1>МОЩНОЕ <br>ПРЕДЛОЖЕНИЕ</h1>
                    <p>Спец. цена на моторы для яхт и больших катеров. <br>Предложение ограниченно.</p>
                    <!--<a href="#">Подробнее</a>-->
                </div>
            </div>
        </div>                
    </div>
</div>
<div id="popular_goods">
    <div class="content">
        <div class="popular_goods">
            <h2>Популярные товары</h2>
            <ul class="goods_list clearfix">
                <?php foreach($query_product as $val): ?>
                        <?php 
                        $select_img=$conn_img->select("(id_product={$val['id_product']}) && (img_product<>'') LIMIT 1");
                        $select_char=$conn_char_prod->selectJoin("t_сharaсter", 'id_character',"id_product={$val['id_product']} LIMIT 3");
                        ?>
                        <li>
                            <div>
                                <img src="<?php echo $select_img[0]['img_product']?>" class="goods_item" alt="">
                                <ul>
                                    <h4><a href="<?php echo $product_uri."/".$val['cpu_product']?>"><?php echo $val['name_product']?></a></h4>
                                    <?php foreach ($select_char as $value): ?>
                                    <li>
                                        <?php echo $value['name_character'].", ".$value['ed_izm'];?>
                                        <span><?php echo $value['value_character_product'];?></span>
                                    </li>
                                    <?php endforeach;?>
                                </ul>
                                <div class="price_basket clearfix">
                                    <div>
                                        <div class="price_side">
                                            <span class="old_price"><?php echo $val['fake_price_product'];?> ₽</span>
                                            <span class="new_price"><?php echo $val['price_product'];?> ₽</span>
                                        </div>
                                        <div class="basket_side">
                                                                <form name="pay">
                        <input type="text" name="id" value="<?php echo $val["id_product"]?>" style="display: none"/>
                                            <!--<a href="#" class="to_basket_btn"></a>-->
                    <input type="submit" value=""/>
                    </form>
<!--                                            <a href="#" class="to_basket_btn"></a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php endforeach; ?>
            </ul>
        </div>
        <div class="text_block">
            <h2>Honda сервис "АКБОР" приветствует вас!</h2>
            <p>При выполнении ремонта и ТО автомобилей используются сервисные продукты и масла рекомендованные производителем автомобилей а также оригинальные запасные часта. Норма времени применяемая при выполнении работ установлена производителями автомобилей и подтверждена стандартами Российской Федерации.Порядок приема автомобилей в ремонт: <br>
Автомобили  оформляются в ремонт на основании заказ - наряда согласованного и подписанного клиентом. <br>
-автомобиль в ремонт принимается в чистом виде ( технологическая мойка обязательна)</p>
            <p>Для выполнения работ принимаются запасные части и расходные материалы предоставленные клиентом а также со склада сервисного центра по ценам предприятия. По окончанию работ оформляется акты выполненных  работ <br>
Оплата работ производиться через кассу предприятия или перечислением денежных  средств на его расчетный счет.</p>
           <!--<a href="#" class="expand_btn">Развернуть</a>-->
        </div>
    </div>
</div>
<?php include 'footer.php'?>
<?php include 'script.php'?>
<script>
    $("#owl_main_slider").owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem : true
    });
</script>
    
</body>
</html>