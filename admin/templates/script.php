<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script  src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script  src="js/jquery.bxslider.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/script.js"></script>
<script>
    $("form[name=pay]").submit(function(){
        var form=$(this);
        
        $.ajax({
                type:'post',
                url: '/payment',
                dataType:'json',
                data: form.serialize()+"&addToCart",
                success: function(data){
                    req=$.parseJSON=data;
                    $("#kol_price").text(req['count']);
                    $("#summ_price").text(req['sum']+" руб.")
                    var n = noty({
                        text        : "Товар добавлен в корзину",
                        type        : "success",
                        dismissQueue: true,
                        timeout     : 1000,
                        //closeWith   : ['click'],
                        layout      : 'bottomLeft',
                        theme       : 'relax',
                        maxVisible  : 10
                    });
                    $("#summ_price").text($("#summ_price").text().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '))
                }
        })
        return false;
    })
    document.addEventListener("DOMContentLoaded",function(){
        $.ajax({
                type:'post',
                url: '/payment',
                dataType:'json',
                data: "view_cart",
                success: function(data){
                    req=$.parseJSON=data;
                    $("#kol_price").text(req['count']);
                    $("#summ_price").text(req['sum']+" руб.")
                    $("#summ_price").text($("#summ_price").text().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '))
                    
                }
        })
    })      
</script>
