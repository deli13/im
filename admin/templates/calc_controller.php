<?php include "config.php";
ini_set("memory_limit", "-1");
$conn_model=new dbquery($connect, 'model');
$conn_mark=new dbquery($connect, 'marka');
//Конфигурация PHPExcel
require_once $root."/admin/asset/PHPExcel/PHPExcel.php";
//require_once $root."/admin/asset/PHPExcel/PHPExcel/Writer/Excel5.php";
require_once $root.'/admin/asset/PHPExcel/PHPExcel/IOFactory.php';
require_once $root.'/admin/asset/PHPExcel/PHPExcel/Writer/IWriter.php';
require_once $root."/admin/asset/vendor/autoload.php";
$input_file=$root."/source/nariad.xlsx";

$mail=new PHPMailer;

$excel= PHPExcel_IOFactory::load($input_file, "Excel2007");
$excel->setActiveSheetIndex(0);


$file=fopen($root.'/source/service.csv', 'r');
$csv_data=array();
while(!feof($file)){
	$m=fgetcsv($file, 100000, ';');
	$csv_data[]=$m;
}
fclose($file);

if ((isset($_POST['reset'])) && ($_POST['reset']!="")){
    session_destroy();
    header("Location: {$calc_uri}");
}


if (isset($_POST['id_mark']) && is_numeric($_POST['id_mark'])){ //Отправка json по ajax
    $id_mark=$_POST['id_mark'];
    $query_model=$conn_model->select("mark_id={$id_mark}");
    echo json_encode($query_model);
}

if ((isset($_POST['name'])) && (isset($_POST['mark'])) && ($_POST["name"]!="")){  //Приёмка формы
    $name= valid_input($_POST['name']);
    $vin= valid_input($_POST['vin']);
    $id_marka= valid_input($_POST['mark']);
    $id_model= valid_input($_POST['model']);
    $probeg= valid_input($_POST['probeg']);
    $phone= valid_input($_POST['phone']);
    $mail= valid_input($_POST['mail']);
    $num_kuz= valid_input($_POST['nkuz']);
    $type_dvig= valid_input($_POST['typedv']);
    $color_kuz= valid_input($_POST['colorkuz']);
    $num_dvig= valid_input($_POST['ndv']);
    $value_key=(isset($_POST['value']))?$_POST['value']:array();
    $query_mark=$conn_mark->select("id={$id_marka}");
    $query_model=$conn_model->select("id={$id_model}");
    $mark=$query_mark[0]['mark'];
    $model=$query_model[0]['model'];

    $excel->getActiveSheet()->getCell("C2")->setValue($name.' '.$phone.' '.$mail);
    $excel->getActiveSheet()->getCell("A5")->setValue($mark.' '.$model);
    $excel->getActiveSheet()->getCell("D6")->setValue($num_kuz);
    $excel->getActiveSheet()->getCell("E6")->setValue($color_kuz);
    $excel->getActiveSheet()->getCell("D8")->setValue($probeg);
    $excel->getActiveSheet()->getCell("F6")->setValue($type_dvig);
    $excel->getActiveSheet()->getCell("H6")->setValue($num_dvig);
    $excel->getActiveSheet()->getCell("D11")->setValue(date("d.m.Y"));
    $summ=0;
    $i=0;
    $base=1250;
    foreach($value_key as $val){
        $newrow=18+$i;
        $i++;
        $itog_line=$csv_data[$val][1]*$base;
        $summ=$summ+$itog_line;
        $excel->getActiveSheet()->insertNewRowBefore($newrow);
        $excel->getActiveSheet()->getCell("A{$newrow}")->setValue($i);
        $excel->getActiveSheet()->getCell("I{$newrow}")->setValue($itog_line);
        $excel->getActiveSheet()->getCell("C{$newrow}")->setValue($csv_data[$val][0]);
    }
    $itog=$newrow+4;
    $excel->getActiveSheet()->getCell("I{$itog}")->setValue($summ);

    $writer= PHPExcel_IOFactory::createWriter($excel, "Excel2007");
    $namefile="nariad".microtime()."-".date("d-m-Y");
    $writer->save($root."/source/{$namefile}.xlsx");
    $_SESSION['excelfile']="/source/".$namefile.".xlsx";
    
    $subject="Письмо с сайта акбор";

                $mail->CharSet="UTF-8";
                $mail->setFrom("info@akbor.spb.ru");
		$mail->addAddress($mail);
                $mail->addAddress("info@akbor.spb.ru");
		$mail->isHTML(true);
		$mail->Subject=$subject;
		$mail->Body="Письмо с сайта Акбор СПБ";
                $mail->AddAttachment($root."/source/{$namefile}.xlsx", $namefile."xlsx");
		if (!$mail->send()){
                    echo "no send";
                    
                } else {
                
                    //echo "send";
                };
    
    header("Location: {$calc_uri}");
    
    
    
}

