<?php include 'config.php';
$id_real=$_SESSION['id_real'];
if (!is_numeric($id_real)) header("Location: http://{$host}");
$realization="t_realization";
$conn_realization=new dbquery($connect, $realization);
$where="id_realization={$id_real}";
$query=$conn_realization->select($where);
$content=$query[0];

$title="";
$description="";
$keywords="";

session_destroy();
?>
<!DOCTYPE html>
<html lang="en">
<?php include 'head_zak.php';?>
<body>
<?php include 'header_zak.php';?>
<div id="order">
    <div class="content">
        <div class="order">
            <h2>Ваш заказ № <?php echo $content['id_realization']." от ". dateNorm($content['created_realization'])?> принят!</h2>
            <h4>В ближайшее время с Вами свяжется сотрудник Интернет-магазина, чтобы уточнить время доставки.</h4>
            <div class="order_block clearfix">
                <div class="ord_left">
                    <table>
                        <tr>
                            <td>Способ получения</td>
                            <td><?php echo $content['dostavka_realization']?>;</td>
                        </tr>
                        <tr>
                            <td>Куда доставить</td>
                            <td><?php echo $content['address_realization']?></td>
                        </tr>
                       <!-- <tr>
                            <td>Когда</td>
                            <td>Сегодня, 01 ноября, вт</td>
                        </tr>-->
                        <tr>
                            <td>Сумма заказа</td>
                            <td><?php echo $content['summ_realization']?> ₽</td>
                        </tr>
                       <!-- <tr>
                            <td>Стоимость доставки</td>
                            <td>490 руб.</td>
                        </tr>
                        <tr>
                            <td>К оплате</td>
                            <td>301 690 ₽</td>
                        </tr>
                        <tr>
                            <td>Способ оплаты</td>
                            <td>наличными</td>
                        </tr>-->
                        <tr>
                            <td>Электронная почта</td>
                            <td><?php echo $content['email_realization']?></td>
                        </tr>
                        <tr>
                            <td>Телефон</td>
                            <td><?php echo $content['phone_realization']?></td>
                        </tr>
                    </table>
                </div>
                <div class="ord_right">
                    <div class="pay_online clearfix">
                        <a href="#">Оплатить онлайн</a>
                        <img src="img/visa_master.png" alt="">
                    </div>
                    <!--<div class="pay_contract">
                        <a href="#">Счет-Контракт</a>
                    </div>-->
                    <div class="to_print">
                        <a href="<?php echo $score_uri.'?id_score='.$content['id_realization'];?>" id="print">Распечатать</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer_zak.php'?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/script.js"></script>
<script>
//var but_print=document.getElementById('print');
//but_print.addEventListener("click", function(e){
//    var evt = e ? e : window.event;
//    (evt.preventDefault) ? evt.preventDefault() : evt.returnValue = false;
//    window.print();
//})
</script>
</body>
</html>