<?php include 'config.php';
$marka='marka';
$model='model';
$conn_mark=new dbquery($connect, $marka);
$query_mark=$conn_mark->select("");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<?php include 'head.php'?>
<body>
<style>
    table {
        width:80%;
        margin-top:20px;
    }
    td{
        padding:5px;
        text-align: left;
    }
</style>
<?php include 'header.php'?>
	<?php

$file=fopen($root.'/source/service.csv', 'r');
$csv_data=array();
while(!feof($file)){
	$m=fgetcsv($file, 100000, ';');
	$csv_data[]=$m;
}
fclose($file);
?>

    
        
        
	  <div id="calc">
	  	<div class='content'>
                    <?php if (!isset($_SESSION['excelfile'])):?>
	 
               <form action="<?php echo $calccontroller_uri?>" method="POST">
                   <table border="0">
                       <tbody>
                           <tr>
                       <td><input type='text' id="name" name='name' placeholder='Имя'></td>
                       <td><input type="text" id="vin" name="vin" placeholder="VIN номер"/></td>
                           </tr>
                           <tr>
                       <td>          <select name="mark" id="marka">
              <option selected>Марка</option>
              <?php foreach ($query_mark as $val): ?>
                  <option value="<?php echo $val['id']?>"><?php echo $val['mark']?></option>
             <?php endforeach; ?>
          </select></td>
                       <td>
                           <input type="text" id="probeg" name="probeg" placeholder="Пробег"/>
                       </td>
                           </tr>
                           <tr>
                       <td>
                          <select name="model" id="model">
                              <option selected>Модель</option>
                          </select>
                       </td>
                       <td>
                           <input type="tel" id="phone" name="phone" placeholder="Телефон"/>
                       </td>
                           </tr>
                           <tr>
                               <td colspan="2"><input type='email' name='mail' id="mail" placeholder="email"></td>
                           </tr>
                           <tr>
                               <td>
                                   <input type="text" name="nkuz" id="nkuz" placeholder="Номер кузова"/>
                               </td>
                               <td>
                                   <input type="text" name="typedv" id="typedv" placeholder="Тип двигателя">
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    <input type="text" name="colorkuz" id="colorkuz" placeholder="Цвет кузова"/>
                               </td>
                               <td>
                                   <input type="text" id="ndv" name="ndv" placeholder="Номер двигателя"/>
                               </td>
                           </tr>
                   </tbody>
                   </table>
                   <input type="submit" value="Рассчитать" style="width: 300px;height:40px;">
<table border=0>
	<?php
	foreach($csv_data as $key=>$val){
		echo "<tr>";
		if ($val[1]==""){
			echo "<td style='font-size:20px;'><br>{$val[0]}<br><br></td>";
			continue;
		}
		echo "<td><input type='checkbox' style='-webkit-appearance:checkbox' name='value[]' value='{$key}'/>{$val[0]}</td>";
		echo "</tr>";
	}
	?>
</table>
       
       </form>
                    <?php endif;?>
                    <br/>
                    <br/>
                    <p>Скачайте ваш заказ <a href="<?php echo $_SESSION['excelfile']; ?>">Файл</a></p>
                    <br/>
                    <input type="submit" id="reset" name="reset" value="Рассчитать снова"/>
                    <br/>
                    <br/>
        </div>
</div>
    <?php include 'footer.php'?>
    <?php include 'script.php'?>

</body>
<script>
    
var mark = document.getElementById('marka');
mark.addEventListener('change', function(){
  var index = this.selectedIndex;
  $.ajax({
      type:"post",
      url:'<?php echo $calccontroller_uri?>',
      dataType:'json',
      data:'id_mark='+index,
      success: function(data){
          var dat=$.parseJSON=data;
          $("#model").empty();
          $("#model").append($("<option selected>Модель</option>"));
          for(var val in dat){
              $("#model").append($("<option value='"+dat[val]['id']+"'>"+dat[val]['model']+"</option>"));
          }
      }
  })
})
</script>
<script>
var forms=document.forms[0];
var names=document.getElementById("name");
var vin=document.getElementById("vin");
var marka=document.getElementById("marka");
var probeg=document.getElementById("probeg");
var model=document.getElementById("model");
var phone=document.getElementById("phone");
var email=document.getElementById("mail");
var nkuz=document.getElementById("nkuz");
var typedv=document.getElementById("typedv");
var colorkuz=document.getElementById("colorkuz");
var ndv=document.getElementById("ndv");

forms.onsubmit=function(){
    var postform=true;
    if ((names.value=="") || (vin.value=="") || (marka.value=="")){
        console.log("false")
        postform=false;
    }
    if ((probeg.value=="") || (model.value=="") || (phone.value=="")){
        console.log("false")
        postform=false;
    }
    if ((email.value=="") || (nkuz.value=="") || (typedv.value=="")){
        console.log("false")
        postform=false;
    }
    if ((colorkuz.value=="") || (ndv.value=="")){
        console.log("false")
        postform=false;
    }
    if (postform==false) alert("Не заполнены обязательные поля");
    return postform
}
</script>
<script>
var reset=document.getElementById("reset");
reset.addEventListener("click", function(){
    var reset_name=document.createElement("input");
    reset_name.setAttribute("name", "reset");
    reset_name.value=1;
    var forms=document.createElement("form");
    forms.setAttribute("method", "post");
    forms.setAttribute("action", "<?php echo $calccontroller_uri?>");
    forms.appendChild(reset);
    forms.appendChild(reset_name);
    document.body.appendChild(forms);
    forms.submit();
})
</script>
</html>





