<?php 
include 'config.php';

$t_catalog="t_catalog";
$t_product="t_product";
$t_char_prod="t_character_product";
$t_img="t_img_product";
$t_char="t_сharaсter";
$conn_catalog=new dbquery($connect, $t_catalog);
$conn_product=new dbquery($connect, $t_product);
$conn_char_prod=new dbquery($connect, $t_char_prod);
$conn_img=new dbquery($connect, $t_img);
$conn_char=new dbquery($connect, $t_char);
if ((isset($_GET['page'])) && (is_numeric($_GET['page'])) || isset($_GET['sort'])){
    $page=(isset($_GET['sort']))?0:$_GET['page'];
    $cpu_get=explode("?",$cpu);
    $cpu=$cpu_get[0];
}

$cpu_catalog=$connect->quote($cpu);
$query_catalog=$conn_catalog->select("cpu_catalog={$cpu_catalog}");
if(count($query_catalog)<1){ //Если каталог не найден
    header("Location: /404");
}

/////Ебучие TDK/////
$title=$query_catalog[0]['title_catalog'];
$description=$query_catalog[0]['description_catalog'];
$keywords=$query_catalog[0]['keywords_catalog'];
if ($query_catalog[0]['parent_catalog']!=""){
$parent_query=$conn_catalog->select("id_catalog={$query_catalog[0]['parent_catalog']}");
}
$parent_catalog_query=$conn_catalog->select("parent_catalog={$query_catalog[0]['id_catalog']}");




/////Отбор товаров+пагинация/////
if ((isset($_POST["requeststring"])) && ($_POST["requeststring"]!="")){ //если фильтрация задана
    $search_array= json_decode($_POST["requeststring"], 1);
    $where="(";
    $ij=0;
    foreach($search_array as $val){  //Формирование условия
        if ($ij!=0){
            $where.=" or ";
        }
        $where.="((t_character_product.id_character='{$val[0]}') and (t_character_product.value_character_product='{$val[1]}'))";
        $ij++;
    }
    
    $where.=")";    
    $query_product=$conn_product->selectJoin("t_character_product", "id_product", $where);
    
        } else { //Если фильтрация не задана
    


    $where="id_catalog={$query_catalog[0]['id_catalog']}";
    $query_count_product=$conn_product->select($where);
    $count_product=count($query_count_product);
    $current_page=1;
    $count_product_page=9;
    if ((isset($page)) && ($page>0)){
        $current_page=$page;
    }
    $start=($current_page-1)*$count_product_page;
    
    if ((isset($_GET['sort']) && ($_GET['sort']!=""))){
        $sort_type= filter_input(INPUT_GET, "sort", FILTER_SANITIZE_STRING);
        switch($sort_type){
            case "pricedown":
                $where.=" order by price_product+0 desc";
                break;
            case "priceup":
                $where .=" order by price_product+0 asc";
                break;
            case "alphdown":
                $where.=" order by name_product desc ";
                break;
            case "alphup":
                $where.=" order by name_product asc ";
                break;
        }
    }
    $where.=" LIMIT {$start}, {$count_product_page}";
    $query_product=$conn_product->selectColumn('SQL_CALC_FOUND_ROWS *', $where);
    $page=0;
    
    
    
}
//Характеристики
$query_char=$conn_char->select("id_catalog={$query_catalog[0]['id_catalog']}");

?>

<!DOCTYPE html>
<html lang="en">
<?php include 'head.php'?>
<body>
<?php include 'header.php' ?>
<!--<div id="main_slider">
    <div id="owl_main_slider" class="owl-carousel">
        <div class="item">
            <img src="img/main_bg.jpg" alt="" class="backfround">
            <div class="content">
                <div class="item_block">
                    <h1>МОЩНОЕ <br>ПРЕДЛОЖЕНИЕ</h1>
                    <p>Спец. цена на моторы для яхт и больших катеров. <br> Моторы на складе ООО «Хонда Мотор РУС», <br>заказать можно через дилеров. <br>Предложение ограниченно.</p>
                    <a href="#">Подробнее</a>
                </div>
            </div>
        </div>
        <div class="item">
            <img src="img/main_bg.jpg" alt="" class="backfround">
            <div class="content">
                <div class="item_block">
                    <h1>МОЩНОЕ <br>ПРЕДЛОЖЕНИЕ</h1>
                    <p>Спец. цена на моторы для яхт и больших катеров. <br> Моторы на складе ООО «Хонда Мотор РУС», <br>заказать можно через дилеров. <br>Предложение ограниченно.</p>
                    <a href="#">Подробнее</a>
                </div>
            </div>
        </div>
        <div class="item">
            <img src="img/main_bg.jpg" alt="" class="backfround">
            <div class="content">
                <div class="item_block">
                    <h1>МОЩНОЕ <br>ПРЕДЛОЖЕНИЕ</h1>
                    <p>Спец. цена на моторы для яхт и больших катеров. <br> Моторы на складе ООО «Хонда Мотор РУС», <br>заказать можно через дилеров. <br>Предложение ограниченно.</p>
                    <a href="#">Подробнее</a>
                </div>
            </div>
        </div>
    </div>
</div>-->
<div id="catalog">
    <div class="content">
        <div class="catalog">
            <div class="breadcrumbs">
                <ul class='clearfix'>
                    <li><a href="/">Главная</a></li>
                    <?php if((isset($parent_query)) && (count($parent_query)!=0)):?>
                    <li><a href="<?php echo $catalog_uri.'/'.$parent_query[0]['cpu_catalog']?>"><?php echo $parent_query[0]['name_catalog']?></span></a></li>
                    <?php endif;?>
                    <li><span><?php echo $query_catalog[0]['name_catalog']?></span></li>
                </ul>
            </div>
            <div class="catalog_main clearfix">
                <div class="catalog_left">
                    <div class="filter_title">
                        <h4><?php echo $query_catalog[0]['name_catalog']?></h4>
                    </div>
               <!--     <div class="accordion">
                        <a href="#">
                            Розничная цена
                            <img src="img/select_down_arr.png" alt="">
                        </a>
                        <div>
                            <p>
                                <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                            </p>
                            <div id="slider-range"></div>
                        </div>
                    </div> -->
                    
                    
                   <?php foreach($query_char as $val): //Вывод доступных характеристик?> 
                    <div class="accordion">
                        <a href="#">
                            <?php echo "{$val['name_character']}, {$val['ed_izm']}"; ?>
                            <img src="img/select_down_arr.png" alt="">
                        </a>
                        <div>
                            <?php
                            $query_char_acc=$conn_char_prod->selectColumn("distinct value_character_product", "id_character={$val['id_character']}");
                            foreach($query_char_acc as $val_char): //Вывод доступных значений
                            ?>
                                <input id="checkbox_<?php echo $val['id_character']."_".$val_char['value_character_product']?>" type="checkbox" name="checkbox">
                                <label for="checkbox_<?php echo $val['id_character']."_".$val_char['value_character_product']?>">
                                    <span></span><?php echo $val_char['value_character_product']; ?>
                                </label>
                            <?php endforeach; ?>
                            
                        </div>
                    </div>
                           <?php endforeach; ?>

               <?php if(count($parent_catalog_query)==0): ?>
               <div class="filter_bottom clearfix">
                        <a href="<?php echo $_SERVER['REQUEST_URI']?>" id="reset_show" class="del_btn">Сбросить</a>
                        <a href="#" id="show" class="show_btn">Показать</a>
                    </div>
                       <?php endif;?>
                  <?php 
$str=explode("/",$_SERVER['REQUEST_URI']);
$str=$str[2];
if($str=='silovaya_tehnika'): ?>
                  Скачать каталог с ценами:<br><br>
                  <a href="silovaja/1.xlsx">Лодочные моторы</a><br><br>
                  <a href="silovaja/2.xlsx">Надувные лодки</a><br><br>
                  <a href="silovaja/3.xlsx">Генераторы</a><br><br>
                  <a href="silovaja/4.xlsx">Водяные насосы</a><br><br>
                  <a href="silovaja/5.xlsx">Газонокасилки</a><br><br>
                  <a href="silovaja/6.xlsx">Газонокасилки с сидением</a><br><br>
                  <a href="silovaja/7.xlsx">Мотокоса</a><br><br>
                  <a href="silovaja/8.xlsx">Культиваторы</a><br><br>
                  <a href="silovaja/9.xlsx">Снегоуборщики</a><br><br>
                  <?php endif;?>                  
                </div>
                <div class="catalog_right">
                    <div class="catalog_right_top clearfix">
                        <?php if(count($parent_catalog_query)==0): ?>
                        <div class="catalog_right_top_left">
                            <span>Сортировать по</span>
                            <select name="sort" id="sort">
                                <option selected disabled>Выбор сортировки</option>
                                <option value="pricedown">Уменьшению цены</option>
                                <option value="priceup">Увелечению цены</option>
                                <option value="alphup">Алфавиту (А-Я)</option>
                                <option value="alphdown">Алфавиту (Я-А)</option>
                             </select>
                        </div>
                        <?php endif;?>
                        <div class="catalog_right_top_right clearfix">
                            <span>Вид:</span>
                            <a href="#" class="first_type_btn active"></a>
                            <a href="#" class="second_type_btn"></a>
                        </div>
                    </div>
                    <ul class="goods_list clearfix">
                        <?php 
                        if ((count($query_product)<1) && (count($parent_catalog_query)==0)) echo "<p>Товаров не найдено</p>";
                        ?>
                        <?php foreach($query_product as $val): ?>
                        <?php 
                        $select_img=$conn_img->select("(id_product={$val['id_product']}) && (img_product<>'') LIMIT 1");
                        $select_char=$conn_char_prod->selectJoin("t_сharaсter", 'id_character',"id_product={$val['id_product']} LIMIT 3");
                        ?>
                        <li>
                            <div>
                                <img src="<?php echo $select_img[0]['img_product']?>" class="goods_item" alt="">
                                <ul>
                                    <h4><a href="<?php echo $product_uri."/".$val['cpu_product']?>"><?php echo $val['name_product']?></a></h4>
                                    <?php foreach ($select_char as $value): ?>
                                    <li>
                                        <?php echo $value['name_character'].", ".$value['ed_izm'];?>
                                        <span><?php echo $value['value_character_product'];?></span>
                                    </li>
                                    <?php endforeach;?>
                                </ul>
                                <div class="price_basket clearfix">
                                    <div>
                                        <div class="price_side">
                                            <span class="old_price"><?php echo $val['fake_price_product'];?> ₽</span>
                                            <span class="new_price"><?php echo $val['price_product'];?> ₽</span>
                                        </div>
                                        <div class="basket_side">
                                            <form name="pay">
                                                <input type="text" name="id" style="display: none" value="<?php echo $val["id_product"]?>"/>
                                            <!--<a href="#" class="to_basket_btn"></a>-->
                                            <input type="submit" value=""/>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php endforeach; ?>
                        <?php if (count($parent_catalog_query)>0):
                                  foreach($parent_catalog_query as $val):?>
                                                    <li>
                            <div>
                                <img src="<?php echo $val['img_catalog']?>" class="goods_item" alt="">
                                <ul>
                                    <h4><a href="<?php echo $catalog_uri."/".$val['cpu_catalog']?>"><?php echo $val['name_catalog']?></a></h4>
                                    
                                </ul>
                                <div class="price_basket clearfix">
                                    <div>
                                        <!--<div class="price_side">
                                            <span class="old_price"> ₽</span>
                                            <span class="new_price"> ₽</span>
                                        </div>
                                        <div class="basket_side">
                                            <a href="#" class="to_basket_btn"></a>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </li>
                      


                        <?php     endforeach;
                              endif;?>
                        
                        
                    </ul>
                    <?php if((count($parent_catalog_query)==0) && (!isset($search_array))): ?>
                    <?php //Пагинация
                    $next=$current_page+1;
                    $prev=$current_page-1;
                    $pages=ceil(($count_product/$count_product_page));
                    $req=explode("?",$_SERVER["REQUEST_URI"]);
                    $request_uri=$req[0]."?page=";
                    if (isset($sort_type)){
                        $request_uri=$req[0]."?sort={$sort_type}&page=";
                    }
                    ?>
                    <div class="goods_navigation">

                        <a href="<?php echo $request_uri.$prev?>" class="nav_prev_btn">
                            <img src="img/nav_arr_left.png" alt="">
                        </a>
                        <ul class="clearfix">
			<?php
			for ($i=1; $i<=$pages; $i++){
				if ($i==$current_page){
					echo "<li><a class='active' href='".$request_uri.$i."'>{$i}</a></li>";
					continue;
				}
				if ((($i+2)==$current_page) or (($i+1)==$current_page) or (($i-2)==$current_page) or (($i-1)==$current_page)){
					echo "<li><a href='".$request_uri.$i."'>{$i}</a></li>";
				}
				
			}
			?>
                        </ul>
                        <a href="<?php echo $request_uri.$next?>" class="nav_next_btn">
                            <img src="img/nav_arr_right.png" alt="">
                        </a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>
<?php include 'script.php' ?>

 <?php if (isset($sort_type)): //Сортировка?>
    <script>
    var sort_type="<?php echo $sort_type ?>"
    $("#sort option[value='"+sort_type+"']").attr("selected", "selected");
    </script>
 <?php endif; ?>
    
    
<script>
    $("#owl_main_slider").owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem : true
    });
</script>
<script>
  $( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 260000,
      values: [ 56000, 260000 ],
      slide: function( event, ui ) {
        $( "#amount" ).val(ui.values[ 0 ] + " - " + ui.values[ 1 ]);
      }
    });
    $( "#amount" ).val($( "#slider-range" ).slider( "values", 0 ) +
      " - " + $( "#slider-range" ).slider( "values", 1 ) );
  } );
  </script>
 <script>
 var sort=document.getElementById("sort");
 sort.addEventListener("change", function(){
     var sort_new=this.cloneNode(true);
     sort_new.value=this.value;
     var form=document.createElement("form");
     form.setAttribute("method", "GET");
     form.setAttribute("action", window.location.href);
     document.body.appendChild(form);
     form.appendChild(sort_new);
     form.submit();
 })
 
 </script>
 <script>
 var show=document.getElementById("show");
 var checkbox=document.getElementsByName("checkbox");
 show.addEventListener("click", function(e){
    var evt = e ? e : window.event; //Сброс события
    (evt.preventDefault) ? evt.preventDefault() : evt.returnValue = false;
    
    var serialize={};
    var count_checkbox=checkbox.length;
    for(var i=0; i<count_checkbox; i++){
        if (checkbox[i].checked){
            var string_id=checkbox[i].getAttribute("id");
            var array_id=string_id.split("_");
            array_id.splice(0,1) //Удаляем 0 элемент
            serialize[i]=array_id;
        }
    }
    var sort_string=JSON.stringify(serialize);
    var input_serialize=document.createElement("input");
    input_serialize.setAttribute("name","requeststring");
    input_serialize.value=sort_string;
    var form=document.createElement("form");
    form.setAttribute("method","POST");
    form.setAttribute("action",window.location.href);
    form.appendChild(input_serialize);
    document.body.appendChild(form);
    form.submit();
 })
 </script>

</body>
</html>