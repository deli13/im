<?php include_once 'config.php';
 require_once $root.'/admin/asset/vendor/autoload.php';
 
 $mail=new PHPMailer; //Подключение класса для отправки писем
 
 if ((isset($_POST['fio']) && ($_POST['fio']!=""))){
     $fio=filter_input(INPUT_POST, 'fio', FILTER_SANITIZE_STRING);
     $email_client= filter_input(INPUT_POST,'email', FILTER_SANITIZE_EMAIL);
     $phone= filter_input(INPUT_POST, "phone",FILTER_SANITIZE_STRING);
     $marka= filter_input(INPUT_POST, "marka", FILTER_SANITIZE_STRING);
     $model= filter_input(INPUT_POST, "model", FILTER_SANITIZE_STRING);
     $age= filter_input(INPUT_POST, "age", FILTER_SANITIZE_NUMBER_INT);
     $probeg= filter_input(INPUT_POST, "probeg", FILTER_SANITIZE_NUMBER_FLOAT);
     $date= filter_input(INPUT_POST, "datetime", FILTER_SANITIZE_STRING);
     $comment= filter_input(INPUT_POST, "comment", FILTER_SANITIZE_STRING);
     
     $text="Запись на ремонт: <br/>"
             . "ФИО: {$fio} <br/>"
             . "email: {$email_client}<br/>"
             . "Контактный телефон: {$phone}<br/>"
             . "Марка автомобиля: {$marka}<br/>"
             . "Модель: {$model}<br/>"
             . "Год выпуска: {$age}<br/>"
             . "Пробег: {$probeg}<br/>"
             . "Описание неисправности: {$comment}<br/>"
             . "Желаемая дата {$date}";
     //Подготовка тела письма
    $mail->CharSet="UTF-8";
    $mail->setFrom(mail_akbor); //Для кого
    $mail->addAddress(mail_akbor);
    $mail->addAddress($email_client);
    $mail->isHTML(true);
    $mail->Subject="Письмо с сайта Акбор honda сервис, запись на ремонт";
    $mail->Body=$text;
    if($mail->send()){ //Отправка
        echo "ваше письмо отправлено";
    }//Ты Справишься!!!!!
    else {
        echo 'Письмо не отправлено '.$mail->ErrorInfo;
    }
     
     
 }