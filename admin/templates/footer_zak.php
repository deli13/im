<div id="bottom_footer">
    <div id="other_bottom">
        <div class="content">
            <div class="other_bottom clearfix">
                <div class="copypast">
                    <p>© 2006–<?php echo date('Y')?> ООО "АКБОР"</p>
                </div>
                <div class="web_studio">
                  <p><a href="http://www.w2you.ru">Разработка и продвижение сайта</a> - Студия Вебтую</p>
                </div>
              Обращаем ваше внимание на то, что данный интернет-сайт носит 
исключительно информационный характер и ни при каких условиях не 
является публичной офертой, согласно Статьи 437 (2) ГК РФ.
            </div>
        </div>
    </div>
</div>
<script>
$('.one_price, span.price, .your_order_total_left span').each(function(){
  $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '))
})
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter42693774 = new Ya.Metrika({
id:42693774,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,
webvisor:true
});
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = "https://mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/42693774" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '6r48Z99tFS';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->