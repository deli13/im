<div id="footer">
    <div class="content">
        <div class="footer">
            <ul class="foo_blocks clearfix">
                <li>
                    <h4>Автосервис</h4>
                    <ul>
                        <li><a href="/calc">Калькулятор</a></li>
                        <li><a href="/uslugi/stoimost_servisnyh_rabot1">Подробный прайс</a></li>
                        <!--<li><a href="/text/">Индустриальная техника</a></li>
                        <li><a href="/text/">Двигатели общего назначения</a></li>-->
                    </ul>
                </li>
                <li>
                    <h4>Лодочные моторы</h4>
                    <ul>
                        <li><a href="/uslugi/remont_lodochnyh_motorov">Сервис лодочных мотров</a></li>
                        <li><a href="/catalog/lodochnye_motory">Продажа лодочных моторов</a></li>
                        <!--<li><a href="/text/">Индустриальная техника</a></li>
                        <li><a href="/text/">Двигатели общего назначения</a></li>-->
                    </ul>
                </li>
                <li>
                    <h4>Силовая техника</h4>
                    <ul>
                        <li><a href="/uslugi/otzyvy">Сервис силовой техники</a></li>
                        <li><a href="/catalog/silovaya_tehnika">Продажа силовой техники</a></li>
                        <!--<li><a href="/text/">Индустриальная техника</a></li>
                        <li><a href="/text/">Двигатели общего назначения</a></li>-->
                    </ul>
                </li>
                <li>
                    <h4>Аксессуары</h4>
                    <ul>
               <!--         <li><a href="/text/">Водно-моторная техника</a></li>
                        <li><a href="/text/">Садовая техника</a></li>
                        <li><a href="/text/">Индустриальная техника</a></li>
                        <li><a href="/text/">Двигатели общего назначения</a></li>-->
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="bottom">
    <div class="content">
        <div class="bottom clearfix">
            <div class="copypast">
                <p>© 2006–<?php echo date("Y");?> ООО "АКБОР"</p>
            </div>
            <div class="web_studio">
                <p><a href="http://w2you.ru">Разработка сайта</a> - Студия Вебтую</p>
            </div><br><br>
          <span style="color:#fff;font-size:12px;">Обращаем ваше внимание на то, что данный интернет-сайт носит 
исключительно информационный характер и ни при каких условиях не 
            является публичной офертой, согласно Статьи 437 (2) ГК РФ.</span>
        </div>
    </div>
</div>
<script>
$('#summ_price, .old_price, .new_price, .spec_price, .req_price').each(function(){
    $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '))
})
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter42693774 = new Ya.Metrika({
id:42693774,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,
webvisor:true
});
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = "https://mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/42693774" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '6r48Z99tFS';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->