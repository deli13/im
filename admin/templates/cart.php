<?php 
include_once 'config.php';

function generateWhere($json, $field){
    $jsonarr= json_decode($json, 1);
    $where="";
    $count=count($jsonarr);
    for ($i=0; $i<$count; $i++){
        if ($i!=($count-1)){
            $where.="({$field}={$jsonarr[$i]}) or ";
        } else {
            $where.="({$field}={$jsonarr[$i]})";
        }
    }
    return $where;
}

$t_product="t_product";
$t_catalog="t_catalog";
$t_char_prod="t_character_product";
$t_char="t_сharaсter";
$t_img="t_img_product";
$conn_product=new dbquery($connect, $t_product);
$conn_char=new dbquery($connect, $t_char_prod);
$conn_img=new dbquery($connect, $t_img);
$cpu_product=$connect->quote($cpu);
$select_product=$conn_product->selectJoin($t_catalog,"id_catalog","cpu_product={$cpu_product}");
if (count($select_product)<1){  //Если товар не найден возвращаемся туда откуда пришли
    header("Location: {$referer}");
}
//print_r($select_product);
$product=$select_product[0];
$id_product=$product["id_product"];
///////Ебучее TDK//////////
$title=$product["title_product"];
$description=$product["description_product"];
$keywords=$product["description_product"];
/////Конец ебучих TDK//////////
$select_char=$conn_char->selectJoin($t_char, "id_character", "id_product={$id_product}");  //Блядские характеристики
//print_r($select_char);
////Картиночки
$query_img=$conn_img->select("(id_product={$id_product}) && (img_product<>'')");
$field_product="id_product";
//С этим товаром покупают и т.п.
$where_required= generateWhere($product["required_product"], $field_product); //генерация условий
$where_personal= generateWhere($product["personal_product"], $field_product);
$required=$conn_product->select($where_required);
$personal=$conn_product->select($where_personal);



?>
<!DOCTYPE html>
<html lang="ru">
<?php include 'head.php' ?>
<body>
<?php include 'header.php' ?>
<div id="cart">
    <div class="content">
        <div class="cart">
            <div class="breadcrumbs">
                <ul class='clearfix'>
                    <li><a href="/">Главная</a></li>
                    <li><a href="<?php echo $catalog_uri."/".$product['cpu_catalog']?>"><?php echo $product['name_catalog']?></a></li>
                    <li><span><?php echo $product['name_product']?></span></li>
                </ul>
            </div>
            <div class="cart_top clearfix">
                <div class="cart_slider">
                    <ul class="bxslider">
                        <?php foreach($query_img as $val):?>
                        <li>
                            <img src="<?php echo $val['img_product']?>" />
                            <span class="triangle new">
                                <span>NEW!</span>
                            </span>
                        </li>
                        <?php endforeach;?>
                    </ul>
                    <div id="bx-pager">
                        <?php $i=0;foreach($query_img as $val):?>
                        <a data-slide-index="<?php echo $i?>" href=""><img src="<?php echo $val['img_product']?>" /></a>
                        <?php $i++; endforeach;?>
                    </div>
                </div>
                <div class="cart_info">
                    <h3><?php echo $product['name_product']?></h3>
                    <p><?php echo $product['description1_product'];?></p>
                    <ul>
                        <?php $i=0; foreach($select_char as $val):?>
                        <li><p><?php echo $val['name_character'].','.$val['ed_izm'];?></p> <span><?php echo $val['value_character_product'];?></span></li>
                        <?php  $i++; if($i==3)break;endforeach; ?>
                    </ul>
                    <a href="<?php echo $product['url_public_product'];?>">Подробные харктеристики</a>
<!--                    <a href="#">Инструкция</a>-->
                </div>
                <div class="cart_prices">
                    <div class="req_block">
                        <span class="req_price_text">Рекомендуемая розничная цена</span>
                        <span class="req_price"><?php echo $product['fake_price_product']?> <i>₽</i></span>
                    </div>
                    <div class="spec_block">
                        <span class="spec_price_text">Специальная цена</span>
                        <span class="spec_price"><?php echo $product['price_product'];?> <i>₽</i></span>
                    </div>
                    <p>Срок акции: <br> с 14 - 25 Ноября</p>
                    
                    <form name="pay">
                        <input type="text" name="id" value="<?php echo $product["id_product"]?>" style="display: none"/>
                                            <!--<a href="#" class="to_basket_btn"></a>-->
                    <input type="submit" value="В корзину"/>
                    </form>
                    
                </div>
            </div>
            <div class="cart_tab tab" id="tab">
                <ul class="tab_navbar clearfix" id="tab_navbar">
                    <li><a href="javascript:void(0);" class="active tab_btn" id="tab1">Характеристики</a></li>
                    <li><a href="javascript:void(0);" id="tab2" class="tab_btn">Описание</a></li>
                    <li><a href="javascript:void(0);" id="tab3" class="tab_btn">Доставка и оплата</a></li>
                    <li><a href="javascript:void(0);" id="tab4" class="tab_btn">Инструкция</a></li>
                    <li><a href="javascript:void(0);" id="tab5" class="tab_btn">Это полезно знать!</a></li>
                </ul>
                <div class="tab_main">
                    <div id="con_tab1" class="tab_blocks active">
                        <ul>
                            <?php  foreach($select_char as $val):?>
                                 <li><p><?php echo $val['name_character'].', '.$val['ed_izm'];?></p> <span><?php echo $val['value_character_product'];?></span></li>
                            <?php  endforeach; ?>
                        </ul>
                    </div>
                    <div id="con_tab2" class="tab_blocks">
                            <?php echo $product['description2_product'];?>
                    </div>
                    <div id="con_tab3" class="tab_blocks">
                        <?php echo $product['description3_product'];?>
                    </div>
                    <div id="con_tab4" class="tab_blocks">
                        <?php echo $product['manual_product'];?>
                    </div>
                    <div id="con_tab5" class="tab_blocks">
                        <?php echo $product['description4_product'];?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="required_items">
    <div class="content">
        <div class="required_items">
            <h2>Необходимые товары</h2>
            <div id="owl_required_items" class="owl-carousel">
                <?php foreach ($required as $val): 
                        $select_img=$conn_img->select("(id_product={$val['id_product']}) && (img_product<>'') LIMIT 1");
                        $select_char=$conn_char->selectJoin("t_сharaсter", 'id_character',"id_product={$val['id_product']} LIMIT 3");
                    ?>
                <div class="item">
                    <div>
                        <img src="<?php echo $select_img[0]['img_product']?>" class="goods_item" alt="">
                      <!--  <img src="img/gift.png" alt="" class="gift">
                        <span class="triangle new">
                            <span>NEW!</span>
                        </span> -->
                        <h4><a href="<?php echo $product_uri.'/'.$val['cpu_product']?>"><?php echo $val['name_product']?></a></h4>
                        <ul>
                                    <?php foreach ($select_char as $value): ?>
                                    <li>
                                        <?php echo $value['name_character'].", ".$value['ed_izm'];?>
                                        <span><?php echo $value['value_character_product'];?></span>
                                    </li>
                                    <?php endforeach;?>
                        </ul>
                        <div class="price_basket clearfix">
                            <div class="price_side">
                                <span class="old_price"><?php echo $val['fake_price_product']?> руб.</span>
                                <span class="new_price"><?php echo $val['price_product']?> руб.</span>
                            </div>
                            <div class="basket_side">
                                <form name="pay">
                                    <input type="text" name="id" style="display: none" value="<?php echo $val["id_product"]?>"/>
                                            <!--<a href="#" class="to_basket_btn"></a>-->
                                    <input type="submit" value=""/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
<?php endforeach; ?>
            </div>
        </div>
    </div>
</div>



<div id="personal_rec">
    <div class="content">
        <div class="personal_rec">
            <h2>Персональные рекомендации</h2>
            <div id="owl_personal_rec" class="owl-carousel">
                
                <?php foreach ($personal as $val): 
                        $select_img=$conn_img->select("(id_product={$val['id_product']}) && (img_product<>'') LIMIT 1");
                        $select_char=$conn_char->selectJoin("t_сharaсter", 'id_character',"id_product={$val['id_product']} LIMIT 3");
                    ?>
                <div class="item">
                    <div>
                        <img src="<?php echo $select_img[0]['img_product']?>" class="goods_item" alt="">
                      <!--  <img src="img/gift.png" alt="" class="gift">
                        <span class="triangle new">
                            <span>NEW!</span>
                        </span> -->
                        <h4><a href="<?php echo $product_uri.'/'.$val['cpu_product']?>"><?php echo $val['name_product']?></a></h4>
                        <ul>
                                    <?php foreach ($select_char as $value): ?>
                                    <li>
                                        <?php echo $value['name_character'].", ".$value['ed_izm'];?>
                                        <span><?php echo $value['value_character_product'];?></span>
                                    </li>
                                    <?php endforeach;?>
                        </ul>
                        <div class="price_basket clearfix">
                            <div class="price_side">
                                <span class="old_price"><?php echo $val['fake_price_product']?> руб.</span>
                                <span class="new_price"><?php echo $val['price_product']?> руб.</span>
                            </div>
                            <div class="basket_side">
                                <form name="pay">
                                    <input type="text" name="id" style="display: none" value="<?php echo $val["id_product"]?>"/>
                                            <!--<a href="#" class="to_basket_btn"></a>-->
                                    <input type="submit" value=""/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
<?php endforeach; ?>

            </div>
        </div>
    </div>
</div>
    
    <!--
<div id="services">
    <div class="content">
        <div class="services">
            <h2>Услуги</h2>
            <div id="owl_services" class="owl-carousel">
                <div class="item">
                    <div>
                        <img src="img/serv1.jpg" alt="" class="goods_item">
                        <span class="triangle new">
                            <span>NEW!</span>
                        </span>
                        <h4>Услуга №1 <br>Название услуги</h4>
                        <div class="price_basket clearfix">
                            <div class="price_side">
                                <span class="old_price">189 900 ₽</span>
                                <span class="new_price">139 900 ₽</span>
                            </div>
                            <div class="basket_side">
                                <a href="#" class="to_basket_btn"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="img/serv2.jpg" alt="" class="goods_item">
                        <span class="triangle new">
                            <span>NEW!</span>
                        </span>
                        <h4>Услуга №2 <br>Название услуги</h4>
                        <div class="price_basket clearfix">
                            <div class="price_side">
                                <span class="old_price">189 900 ₽</span>
                                <span class="new_price">139 900 ₽</span>
                            </div>
                            <div class="basket_side">
                                <a href="#" class="to_basket_btn"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="img/serv1.jpg" alt="" class="goods_item">
                        <span class="triangle new">
                            <span>NEW!</span>
                        </span>
                        <h4>Услуга №3 <br>Название услуги</h4>
                        <div class="price_basket clearfix">
                            <div class="price_side">
                                <span class="old_price">189 900 ₽</span>
                                <span class="new_price">139 900 ₽</span>
                            </div>
                            <div class="basket_side">
                                <a href="#" class="to_basket_btn"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="img/serv2.jpg" alt="" class="goods_item">
                        <span class="triangle new">
                            <span>NEW!</span>
                        </span>
                        <h4>Услуга №4 <br>Название услуги</h4>
                        <div class="price_basket clearfix">
                            <div class="price_side">
                                <span class="old_price">189 900 ₽</span>
                                <span class="new_price">139 900 ₽</span>
                            </div>
                            <div class="basket_side">
                                <a href="#" class="to_basket_btn"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="img/serv1.jpg" alt="" class="goods_item">
                        <span class="triangle new">
                            <span>NEW!</span>
                        </span>
                        <h4>Услуга №5 <br>Название услуги</h4>
                        <div class="price_basket clearfix">
                            <div class="price_side">
                                <span class="old_price">189 900 ₽</span>
                                <span class="new_price">139 900 ₽</span>
                            </div>
                            <div class="basket_side">
                                <a href="#" class="to_basket_btn"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="img/serv2.jpg" alt="" class="goods_item">
                        <span class="triangle new">
                            <span>NEW!</span>
                        </span>
                        <h4>Услуга №6 <br>Название услуги</h4>
                        <div class="price_basket clearfix">
                            <div class="price_side">
                                <span class="old_price">189 900 ₽</span>
                                <span class="new_price">139 900 ₽</span>
                            </div>
                            <div class="basket_side">
                                <a href="#" class="to_basket_btn"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="img/serv1.jpg" alt="" class="goods_item">
                        <span class="triangle new">
                            <span>NEW!</span>
                        </span>
                        <h4>Услуга №7 <br>Название услуги</h4>
                        <div class="price_basket clearfix">
                            <div class="price_side">
                                <span class="old_price">189 900 ₽</span>
                                <span class="new_price">139 900 ₽</span>
                            </div>
                            <div class="basket_side">
                                <a href="#" class="to_basket_btn"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div>
                        <img src="img/serv2.jpg" alt="" class="goods_item">
                        <span class="triangle new">
                            <span>NEW!</span>
                        </span>
                        <h4>Услуга №8 <br>Название услуги</h4>
                        <div class="price_basket clearfix">
                            <div class="price_side">
                                <span class="old_price">189 900 ₽</span>
                                <span class="new_price">139 900 ₽</span>
                            </div>
                            <div class="basket_side">
                                <a href="#" class="to_basket_btn"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->
<?php include 'footer.php'?>

<script src="js/jquery.bxslider.js"></script>
<script src="js/owl.carousel.min.js"></script>


<script>
    
    $('.bxslider').bxSlider({
        pagerCustom: '#bx-pager'
    });
    
</script>
<script>
    $("#owl_required_items").owlCarousel({
        items : 4,
        lazyLoad : true,
        navigation : true
    });
    $("#owl_personal_rec").owlCarousel({
        items : 4,
        lazyLoad : true,
        navigation : true
    });
    $("#owl_services").owlCarousel({
        items : 4,
        lazyLoad : true,
        navigation : true
    });
</script>
<script>
    $("form[name=pay]").submit(function(){
        var form=$(this);
        
        $.ajax({
                type:'post',
                url: '/payment',
                dataType:'json',
                data: form.serialize()+"&addToCart",
                success: function(data){
                    req=$.parseJSON=data;
                    $("#kol_price").text(req['count']);
                    $("#summ_price").text(req['sum']+" руб.")
                    $("#summ_price").text($("#summ_price").text().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '))
                    var n = noty({
                        text        : "Товар добавлен в корзину",
                        type        : "success",
                        dismissQueue: true,
                        timeout     : 1000,
                        //closeWith   : ['click'],
                        layout      : 'bottomLeft',
                        theme       : 'relax',
                        maxVisible  : 10
                    });
                }
        })
        return false;
    })
    document.addEventListener("DOMContentLoaded",function(){
        $.ajax({
                type:'post',
                url: '/payment',
                dataType:'json',
                data: "view_cart",
                success: function(data){
                    req=$.parseJSON=data;
                    $("#kol_price").text(req['count']);
                    $("#summ_price").text(req['sum']+" руб.")
                    $("#summ_price").text($("#summ_price").text().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '))
                    
                }
        })
    })      
</script>
<script src="js/script.js"></script>
</body>
</html>
