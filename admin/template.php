
<html>
    <head>
        <meta charset="utf8">
        <title>Шаблоны</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <!--CODEMIRROR -->
        <link rel="stylesheet" href="codemirror/lib/codemirror.css">
        <link rel="stylesheet" href="codemirror/addon/display/fullscreen/fullscreen.css">
        <script src="codemirror/lib/codemirror.js"></script>
        <script src="codemirror/mode/htmlmixed/htmlmixed.js"></script>
        <script src="codemirror/addon/hint/show-hint.js"></script>
        <script src="codemirror/addon/hint/xml-hint.js"></script>
        <script src="codemirror/addon/hint/html-hint.js"></script>
        <script src="codemirror/mode/xml/xml.js"></script>
        <script src="codemirror/mode/javascript/javascript.js"></script>
        <script src="codemirror/mode/css/css.js"></script>
        <script src="codemirror/addon/display/fullscreen/fullscreen.js"></script>
        <!--CODEMIRROR -->
        <script src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>



    </head>
<?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();
$index="Location: /admin/index.php";
if ($_SESSION["role"]!="admin"){
    header($index);
}

$table_name='t_template';
//страница работы с шаблонами
?>
    <body>
        <?php include './top.php'; ?>
        <div class="row">
        <?php include('./left_menu.php') ?>
             <div class="col-md-10">
                 <div class="table_div">
                 <table class="table">
                     <thead>
                     <th>ID</th>
                     <th>Имя</th>
                     <th>Путь</th>
                     <th>Действие</th>
                     </thead>
                     <tbody>
                         <?php
                         $conn=new dbquery($connect, "t_template"); //Вывод таблицы с доступными шаблонами
                        $query=$conn->select('');
                        // $query=$conn->selectColumn('*');
                        //print_r($query);
                         foreach ($query as $row){
                            echo "<tr><td>{$row['id_template']}</td>";
                            echo "<td>{$row['name_template']}</td>";
                            echo "<td>{$row['path_template']}</td>";
                            echo "<td><form method='post' name='delete' action='/admin/controller/template_controller.php'>" //форма удаленияя
                            ."<input style='display:none' name='id' value={$row['id_template']}>"
                            ."<input style='display:none' name='path' value={$row['path_template']}>"
                            ."<input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                            ."<form name='update'><input style='display:none' name='id' value={$row['id_template']}>" //Форма изменения
                            ."<input style='display:none' name='path' value={$row['path_template']}>"
                            ."<input type='submit' name='update' class='btn btn-success' value='Изменить'> </form></td></tr>";
                            }
                           unset($conn);
                         ?>
                     </tbody>
                 </table>
                 </div>
         <div class="form-group">        
             <form action="/admin/controller/template_controller.php" method="post">
            <input style="display: none" name="id_template" id="id" value="">
            <input style="display: none" name="path_template" id="path" value="">
            <input class="form-control" type="text" name="name_template" id="name" placeholder="Имя шаблона" value=""><br>
            <textarea class="form-control" rows="15" id="content" name="code"></textarea><br>
            <input class="form-control btn-primary" type="submit" value="Сохранить">
        </form>
         </div>
            </div>
    </div>
        <script>
       myTextarea=document.getElementById('content');
       var editor=CodeMirror.fromTextArea(myTextarea, {lineNumbers:true,
    mode: "text/html",
    value: document.documentElement.innerHTML, 
    extraKeys: {
        "F11": function(cm) {
          cm.setOption("fullScreen", !cm.getOption("fullScreen"));
        },
        "Esc": function(cm) {
          if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
        }}});
       $("form[name=update]").submit(function(){
           var $form = $(this);
               $.ajax({
                 type:"post",
                 url:"/admin/controller/template_controller.php",
                 data:$form.serialize()+'&update',
                 dataType: "json",
                 success: function(data){
                     var qwe = $.parseJSON=data;
                     //console.log(qwe);
                     $("#id").val(qwe['id']);
                     $("#name").val(qwe['name']);
                     $("#content").text(qwe['content']);
                     $("#path").val(qwe['path']);
                     editor.setValue($("#content").text());
                 }
               });
           return false});
       

        $('form[name=delete]').submit(function(){
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        })
       

//$('textarea').each(function(){
//    var e = ace.edit(this);
//    e.setTheme("ace/theme/textmate");
//    e.getSession().setMode("ace/mode/javascript");
//    $(this).css('height', '777px')
//});


        </script>
    </body>
</html>


