<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
session();
$table="t_catalog";
$table_charact='t_сharaсter';
$conn=new dbquery($connect, $table);
$conn_charr=new dbquery($connect, $table_charact);
$header="Location: ".$_SERVER["HTTP_REFERER"];
define("noimage", "/images/noimage.png");


if ((isset($_POST['name'])) && ($_POST['id']=="")){
    $name=$_POST['name'];
    $parent=$_POST['parent'];
    $json= json_decode($_POST['json'], true);
    $text=$_POST['text'];
    $title=$_POST['title'];
    $description=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    $img=($_FILES['img']['name']!="")?imageUpload($_FILES['img']['name']):noname;
    $cpu=translit($name);
    if ($parent=="null"){
        $values=array($name, $text, $title, $description, $keywords, $cpu, $img);
        $fields=array("name_catalog", "text_catalog", "title_catalog", "description_catalog", "keywords_catalog", "cpu_catalog", "img_catalog");    
    } else {
        $values=array($name, $text, $title, $description, $keywords, $parent, $cpu, $img);
        $fields=array("name_catalog", "text_catalog", "title_catalog", "description_catalog", "keywords_catalog", "parent_catalog", "cpu_catalog", "img_catalog");
    }
    $insert_id=$conn->insert($fields, $values);
    $field_char=array("name_character", "ed_izm", "id_catalog");
    foreach ($json as $key=>$value){
        $value_char=array($key, $value[0], $insert_id);
        $conn_charr->insert($field_char, $value_char);
    }
    header($header);
}

if (isset($_POST['delete'])){
    $id=$_POST['id'];
    $where="parent_catalog={$id}";
    $query=$conn->select($where);
    $valid=(count($query)>0)?false:true;
    $product_table="t_product";
    $conn_product=new dbquery($connect, $product_table);
    $query_product=$conn_product->select("id_catalog=$id");
    $valid2=(count($query_product)>0)?false:true;
    if (($valid==false) || ($valid2==false)){ //Проверка на связанные данные
        echo 'Невозможно удалить данную запись. Она используется в связанной таблице <br/>';
        echo '<a href="/admin/catalog.php"> Вернуться к странице шаблонов</a>' ;
        die();
    }
    $conn_charr->delete("id_catalog={$id}");
    $conn->delete("id_catalog={$id}");
    header($header);
}

if (isset($_POST['update']) && ($_POST["update"]=="")){
    $id=$_POST['id'];
    $query=$conn->select("id_catalog={$id}");
    $query_charr=$conn_charr->select("id_catalog={$id}");
    $query=$query[0];
    $json_arr=array("id"=>$query["id_catalog"], "name"=>$query["name_catalog"], "parent"=>$query["parent_catalog"], "text"=>$query["text_catalog"], "title"=>$query["title_catalog"], "description"=>$query["description_catalog"], "keywords"=>$query["keywords_catalog"], "cpu"=>$query['cpu_catalog'], "img"=>$query['img_catalog']);
    foreach($query_charr as $value){
        $json_ch_arr[$value["name_character"]]=array($value["ed_izm"], $value["id_character"]);
    }
    @$json_arr["charact"]=$json_ch_arr;
    echo json_encode($json_arr);
}

if ((isset($_POST['name'])) && ($_POST['id']!="")){
    $id=$_POST['id'];
    $img=noimage;
    $query=$conn->select("id_catalog={$id}");
    if (($query[0]['img_catalog']!=noimage) && ($_FILES['img']['name']!='')){
        unlink("../..".$query[0]['img_catalog']);
    }
    $img=($_FILES['img']['name']!="")?imageUpload($_FILES['img']):noimage;
    $name=$_POST['name'];
    $text=$_POST['text'];
    $json= json_decode($_POST['json'], true);
    $parent=$_POST['parent'];
    $text=$_POST['text'];
    $title=$_POST['title'];
    $description=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    $cpu=translit($name);
    if ($parent=="null"){
        $values=array($name, $text, $title, $description, $keywords, $cpu, $img);
        $fields=array("name_catalog", "text_catalog", "title_catalog", "description_catalog", "keywords_catalog", "cpu_catalog", 'img_catalog');    
    } else {
        $values=array($name, $text, $title, $description, $keywords, $parent, $cpu,$img);
        $fields=array("name_catalog", "text_catalog", "title_catalog", "description_catalog", "keywords_catalog", "parent_catalog", "cpu_catalog", 'img_catalog');
    }
    $where="id_catalog={$id}";
    $conn->update($fields, $values, $where);
    $field_char=array("name_character", "ed_izm");
    print_r($json);
    foreach ($json as $key=>$value){
        $where="id_character='{$value[1]}'";
        print_r($conn_charr->select($where));
        if (sizeof($conn_charr->select($where))==0){
            $conn_charr->insert(array("name_character", 'ed_izm', 'id_catalog'), array($key, $value[0], $id));
        } else{
            $conn_charr->update($field_char, array($key, $value[0]), $where);
        }
        
    }
    header($header);
}
