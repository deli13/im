<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
define("noimage", "/images/noimage.png");
$header="Location: ".$_SERVER["HTTP_REFERER"];
session();
$t_service="t_service";
$conn_service=new dbquery($connect, $t_service);


if (!is_dir('../../images')){
    mkdir('../../images');
}

if ((isset($_POST['name'])) && ($_POST['id']=='')){
    $name=$_POST['name'];
    $text=$_POST['text'];
    $img=($_FILES['img-file']['name']!="")?imageUpload($_FILES['img-file']):noimage;
    $price=$_POST['price'];
    $fake=$_POST['fake'];
    $title=$_POST['title'];
    $description=$_POST['description'];
    $keywords=$_POST['keywords'];
    $user=$_SESSION['id'];
    $field=array("name_service", "text_service", "img_service", "price_service", "fake_service", 
        "title_service", "desccription_service", "keywords_service", "id_user", "cpu_service");
    $value=array($name, $text, $img, $price, $fake, $title, $description, $keywords, $user, translit($name));
    $conn_service->insert($field, $value);
    header($header);
}

if((isset($_POST['update'])) && ($_POST['update']=="")){
    $id=$_POST['id'];
    $query=$conn_service->select("id_service={$id}");
    $name=$query[0]['name_service'];
    $text=$query[0]['text_service'];
    $img=$query[0]['img_service'];
    $price=$query[0]['price_service'];
    $fake=$query[0]['fake_service'];
    $title=$query[0]['title_service'];
    $description=$query[0]['desccription_service'];
    $keywords=$query[0]['keywords_service'];
    $json=array("id"=>$id, "name"=>$name, "text"=>$text, "img"=>$img, "price"=>$price, 
        "fake"=>$fake, "title"=>$title, "description"=>$description, "keywords"=>$keywords);
    echo json_encode($json);
}

if (isset($_POST['delete'])){
    $id=$_POST['id'];
    $where="id_service={$id}";
    $query=$conn_service->select($where);
    if (($query[0]['img_service']!=noimage) && ($query[0]['img_service']!="")){
        unlink("../..".$query_service[0]['img_service']);
    }
    $conn_service->delete($where);
    header($header);
}

if ((isset($_POST['name'])) && ($_POST['id']!="")){
    $id=$_POST['id'];
    $where="id_service={$id}";
    $query=$conn_service->select($where);
    $name=$_POST['name'];
    $text=$_POST['text'];
    if (($query[0]['img_service']!=noname) && ($_FILES['img-file']['name']!="")){
        unlink("../..".$query[0]['img_service']);  
    }
    $img=($_FILES['img-file']['name']!="")?imageUpload($_FILES['img-file']):$query[0]['img_service'];
    $price=$_POST['price'];
    $fake=$_POST['fake'];
    $title=$_POST['title'];
    $description=$_POST['description'];
    $keywords=$_POST['keywords'];
    $user=$_SESSION['id'];
    $field=array("name_service", "text_service", "img_service", "price_service", "fake_service", 
        "title_service", "desccription_service", "keywords_service", "id_user", "cpu_service", "update_service");
    $value=array($name, $text, $img, $price, $fake, $title, $description, $keywords, $user, translit($name), date("d.m.Y"));
    $conn_service->update($field, $value, $where);
    header($header);
}
