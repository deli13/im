<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
$table_name='t_template';
$conn=new dbquery($connect, $table_name);
$header="Location: ".$_SERVER["HTTP_REFERER"];


    if (!is_dir('../templates')){ //Создание папки шаблонов если она не существует
        mkdir('../templates');
    }

if ((isset($_POST['name_template'])) && ($_POST['id_template']=="")){ //создание нового шаблона
    $template= new dbquery($connect, $table_name);
    $code_template=$_POST['code'];
    $name_template=$_POST['name_template'];
    $path='../templates/'.translit($name_template).'.php';
    

    if (!file_exists($path)){  //если файла не существует то создать файл, записать данные, и сделать запись в БД
        $template=fopen($path, 'w+');
        fwrite($template, $code_template);
        if (file_exists($path)){
            $field=array('name_template', 'path_template');
            $values=array($name_template, $path);
            $conn->insert($field, $values);
        }
        fclose($template);
        header($header);
    }
    echo "Такой шаблон уже существует<br />";
    echo '<a href="/admin/template.php"> Вернуться к странице шаблонов</a>' ;
}
if (isset($_POST['delete'])){ //Удаление
    $id_delete=$_POST['id'];
    $conn_cat=new dbquery($connect, 't_category');
    $query_cat=$conn_cat->select('id_template='.$id_delete); //Проверка на связанную таблицу
    if (count($query_cat)>=1){
        echo 'Невозможно удалить данную запись. Она используется в связанной таблице <br/>';
        echo '<a href="/admin/template.php"> Вернуться к странице шаблонов</a>' ;
        die();
    } else {
        $query=$conn->delete("id_template=".$id_delete);
        unlink($_POST['path']);
        header($header);
        unset($conn);
    }
}

if (isset($_POST['update']) && ($_POST["update"]=="")){  //Возвращает JSON файл с данными для AJAX обновления 
    $id_update=$_POST['id'];
    $result=$conn->select('id_template='.$id_update);
    $id=$result[0]['id_template'];
    $name=$result[0]['name_template'];
    $file=$result[0]['path_template'];
    if (file_exists($file)){
        $template=fopen($file, 'r');
        $content=fread($template, filesize($file));
        fclose($template);
    }
    $jsonarr=array('id'=>$id, 'name'=>$name, 'content'=>$content, 'path'=>$file);
     echo json_encode($jsonarr);
}

if ((isset($_POST['name_template'])) && ($_POST['id_template']!="")){ //Update файла и названия шаблона
    $id=$_POST['id_template'];
    $file=$_POST['path_template'];
    $content=$_POST['code'];
    $name=$_POST['name_template'];
    if (file_exists($file)){
        $template=fopen($file, 'w+');
        fwrite($template, $content);
        $conn->update('name_template', $name, 'id_template='.$id);
        fclose($template);
    }
    header($header);
}
?>

