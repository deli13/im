<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
$header="Location: ".$_SERVER["HTTP_REFERER"];
$route_json="../../route.json";
$json=  file_get_contents($route_json);
$json_arr=  json_decode($json,true);

if (isset($_POST["select"])){
    echo $json;
}

if (isset($_POST['delete'])){
    $id=$_POST['id'];
    unset($json_arr[$id]);
    $file=fopen($route_json, "w+");
    fwrite($file, json_encode($json_arr));
    fclose($file);
    header($header);
}

if (isset($_POST["new"])){
    $path=$_POST['path'];
    $url=$_POST['url'];
    $path=array_pop(explode('/', $path));
    $json_arr[$url] = $path;
    $file=fopen($route_json, "w+");
    fwrite($file, json_encode($json_arr));
    fclose($file);
    header($header);
}