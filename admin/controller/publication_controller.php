<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
define("noimage", "/images/noimage.png");

$table='t_publication';
$table_brand='t_brand';
$table_img='t_img';
$table_category='t_category';
$table_hashtag='t_hashtag_publication';
$table_content='t_content';
$conn=new dbquery($connect, $table);
$conn_img=new dbquery($connect, $table_img);
$header="Location: ".$_SERVER["HTTP_REFERER"];

if ((isset($_POST['name'])) && ($_POST['id']=="")){
    $name=$_POST['name'];
    $description=$_POST['descr'];
    $category=$_POST['id_category'];
    $brand=(isset($_POST['id_brand']))?$_POST['id_brand']:'';
    $price=$_POST['price'];
    $otziv=$_FILES['file'];
    $title=$_POST['title'];
    $descr_seo=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    $main=(bool)$_POST['main'];
    $otziv=  imageUpload($otziv);
    $cpu=  translit($name);
    if ($otziv==false) $otziv=noimage;
    $field=array('name_publication','description','id_category','id_brand','price', 'otziv_publication', 'cpu', 'title_publication', 'descr_publication', 'keywords_publication');
    $values=array($name, $description, $category, $brand, $price,$otziv, $cpu, $title, $descr_seo, $keywords);
    $newid=$conn->insert($field, $values);
    //echo $name;
    header($header."?public=".$newid);
}

if(isset($_POST['delete'])){
    $id=$_POST['id'];
    $conn_content=new dbquery($connect, $table_content);
    $conn_hashtag=new dbquery($connect, $table_hashtag);
    $return=$conn->select("id_publication=".$id);
    $conn_content->delete("id_publication=".$id);
    $conn_hashtag->delete("id_publication=".$id);
    $conn->delete("id_publication=".$id);
	$url='http://'.$_SERVER["HTTP_HOST"].'/admin/controller/image-controller.php';
	$param_img=array('delete'=>1, 'id'=>$return[0]['id_img']);
	echo $url;
	print_r($param_img);
    if ($return[0]['id_img']!='') echo postToUrl($url,$param_img);
    if ($return[0]['otziv_publication']!=noimage) unlink('../../'.$return[0]['otziv_publication']);
	
    header($header);
}

if (isset($_POST['update']) && ($_POST["update"]=="")){
    $id=$_POST['id'];
    $query=$conn->select("id_publication=".$id);
    $name=$query[0]['name_publication'];
    $descr=$query[0]['description'];
    $id_cat=$query[0]['id_category'];
    $id_brand=$query[0]['id_brand'];
    $otziv=$query[0]['otziv_publication'];
    $id_img=$query[0]['id_img'];
    $price=$query[0]['price'];
    $title=$query[0]['title_publication'];
    $descr_seo=$query[0]['descr_publication'];
    $keywords=$query[0]['keywords_publication'];
    $json=array('id'=>$id, 'name'=>$name, 'descr'=>$descr, 'category'=>$id_cat, 'brand'=>$id_brand, 'otziv'=>$otziv, 'img'=>$id_img, 'price'=>$price, 'title'=>$title, 'descr_seo'=>$descr_seo, 'keywords'=>$keywords);
    echo json_encode($json);
}

if ((isset($_POST['name'])) && ($_POST['id']!='')){
    $id=$_POST['id'];
    $name=$_POST['name'];
    $description=$_POST['descr'];
    $category=$_POST['id_category'];
    $brand=(isset($_POST['id_brand']))?$_POST['id_brand']:'';
    $price=$_POST['price'];
    $cpu=  translit($name);
    $title=$_POST['title'];
    $descr_seo=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    if ($_FILES['file']['name']!=""){
        $res=$conn->select("id_publication=".$id);
        if ($res[0]['otziv_publication']!=noimage) unlink("../..".$res[0]['otziv_publication']);
        $files=  imageUpload($_FILES['file']);
        $conn->update('otziv_publication', $files, 'id_publication='.$id);
    }
    $field=array('name_publication', 'description', 'id_category', 'id_brand', 'price', 'cpu', "updated_publication", 'title_publication', 'descr_publication', 'keywords_publication');
    $value=array($name, $description, $category, $brand, $price, $cpu, date("Y-m-d H:i:s"), $title, $descr_seo, $keywords);
    $conn->update($field, $value, 'id_publication='.$id);
    header($header);
}

?>
