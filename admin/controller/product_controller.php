<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
define("noimage", "/images/noimage.png");
$header="Location: ".$_SERVER["HTTP_REFERER"];
$table='t_product';
$table_character="t_сharaсter";
$table_char_product='t_character_product';
$table_image="t_img_product";
$conn=new dbquery($connect, $table);
$conn_char=new dbquery($connect, $table_character);
$conn_char_prod=new dbquery($connect, $table_char_product);
$conn_image=new dbquery($connect, $table_image);
session();

if (!is_dir('../../images')){
    mkdir('../../images');
}

if ((isset($_POST['character_id'])) && ($_POST['character_id']!="")){  //Вывод инфы по характеристикам товара AJAX
    $id=(int)$_POST['character_id'];
    $query_char=$conn_char->select("id_catalog='{$id}'");
    $json_arr=array();
    foreach ($query_char as $val){
        $json_arr[$val['id_character']]=array($val['name_character'], $val['ed_izm']);
    }
    echo json_encode($json_arr);
}

if ((isset($_POST['id_img_delete'])) && ($_POST['id_img_delete']!="")){
    $id_img=(int)$_POST['id_img_delete'];
    $where="id_img_product={$id_img}";
    $query=$conn_image->select($where);
    $conn_image->delete($where);
    unlink("../..".$query[0]['img_product']);
    echo "Удалено";
}

if ((isset($_POST["name"])) && ($_POST['id']=="")){ // Добавление
    $name=$_POST['name'];
    $text1=$_POST['desk1'];
    $text2=$_POST['desk2'];
    $text3=$_POST['desk3'];
    $link=$_POST['link'];
    $id_catalog=$_POST['catalog'];
    $manual=$_POST['manual'];
    $text4=$_POST['desk4'];
    $price=$_POST['price'];
    $fake_price=$_POST['fake'];
    $title=$_POST['title'];
    $description=$_POST['descr'];
    $keywords=$_POST['keywords'];
    $main=(isset($_POST['main']))?1:0;
    $service=$_POST['service_json'];
    $required=$_POST["required_json"];
    $personal=$_POST["personal_json"];
    $json= json_decode($_POST['json'], true);
    $fields=array("name_product", "description1_product", 'description2_product', "description3_product",
        "url_public_product", "cpu_product", "id_user", "id_catalog", "price_product",
        "fake_price_product", "title_product", "description_product", "keywords_product",
        "description4_product", "manual_product", "required_product", "personal_product", "viv_main", 'service_product');
    $value=array($name, $text1, $text2, $text3, $link, translit($name), $_SESSION['id'], $id_catalog,
        $price, $fake_price, $title, $description, $keywords, $text4, $manual,$required,$personal, $main, $service);
    $id=$conn->insert($fields, $value);
    $fields=array("value_character_product", "id_product", "id_character");
    foreach ($json as $key=>$value){
        $values=array($value, $id, $key);
        $conn_char_prod->insert($fields, $values);
    }
    $field=array("img_product", "id_product");
    if ($_FILES['images']!=""){  //Загрузка файла
        $file= fileArray($_FILES['images']);
        foreach ($file as $files){
            $name_img= imageUpload($files);
            $values=array($name_img, $id);
            $conn_image->insert($field, $values);
        }
    }
    
   header($header);
}

if (isset($_POST['delete'])){ //удаление
    $id=$_POST['id'];
    $conn_char_prod->delete("id_product={$id}");  //Удаление связанных записей;

    $select_image=$conn_image->select("id_product={$id}");
    foreach ($select_image as $val){
        unlink("../..".$val['img_product']);
    }
    $conn_image->delete("id_product={$id}");
    $conn->delete("id_product={$id}");
    header($header);
}

if (isset($_POST['update']) && ($_POST["update"]=="")){  //Запрос апдейтаd
    $id=$_POST['id'];
    $query_product=$conn->select("id_product={$id}");
    $query_char=$conn_char_prod->selectJoin('t_сharaсter','id_character',"id_product={$id}");
    $query_img=$conn_image->select("id_product={$id}");
    $prod=$query_product[0];
    $json_arr=array("id"=>$id,"name"=>$prod['name_product'], "desk1"=>$prod['description1_product'], "desk2"=>$prod['description2_product'],
        "desk3"=>$prod['description3_product'], "url"=>$prod['url_public_product'], "price"=>$prod['price_product'], "fake"=>$prod["fake_price_product"],
        "title"=>$prod['title_product'], "description"=>$prod['description_product'],"catalog"=>$prod['id_catalog'], "keywords"=>$prod['keywords_product'],
        "desk4"=>$prod['description4_product'], "manual"=>$prod['manual_product'], "main"=>$prod['viv_main']);
    $json_arr[]=$query_char;
    @$json_arr['img']=$query_img;
    $json_arr["personal"]=json_decode($prod['personal_product'], 1);
    $json_arr["require"]= json_decode($prod['required_product'],1);
    $json_arr["service"]= json_decode($prod['service_product'],1);
    echo json_encode($json_arr);
}

if ((isset($_POST['name'])) && ($_POST['id']!="")){
    $id=$_POST['id'];
    $name=$_POST['name'];
    $text1=$_POST['desk1'];
    $text2=$_POST['desk2'];
    $text3=$_POST['desk3'];
    $link=$_POST['link'];
    $id_catalog=$_POST['catalog'];
    $price=$_POST['price'];
    $fake_price=$_POST['fake'];
    $title=$_POST['title'];
    $description=$_POST['descr'];
    $keywords=$_POST['keywords'];
    $manual=$_POST['manual'];
    $text4=$_POST['desk4'];
    $required=$_POST["required_json"];
    $personal=$_POST["personal_json"];
    $service=$_POST['service_json'];
    $main=(isset($_POST['main']))?1:0;
    $where="id_product={$id}";
    $json= json_decode($_POST['json'], true);
    $fields=array("name_product", "description1_product", 'description2_product', "description3_product",
        "url_public_product", "cpu_product", "id_user", "id_catalog", "price_product",
        "fake_price_product", "title_product", "description_product", "keywords_product",
        "description4_product", "manual_product", "required_product", "personal_product", "viv_main", 'service_product', 'update_product');
    $value=array($name, $text1, $text2, $text3, $link, translit($name), $_SESSION['id'], $id_catalog,
        $price, $fake_price, $title, $description, $keywords, $text4, $manual, $required, $personal, $main,$service, date("Y-m-d H:i:s"));
    $conn->update($fields, $value, $where);
    foreach($json as $key=>$val){ //Обновление данных
        $where="id_product={$id} and id_character={$key}";
        $conn_char_prod->update("value_character_product", $val, $where);
    }
    if ($_FILES['images']!=""){  //Загрузка файла
        $field=array("img_product", "id_product");
        $file= fileArray($_FILES['images']);
        foreach ($file as $files){
            $name_img= imageUpload($files);
            $values=array($name_img, $id);
            $conn_image->insert($field, $values);
        }
    }
    $header=explode("?",$_SERVER["HTTP_REFERER"]);
    header("Location: {$header[0]}"."?id={$id}");
}

