<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
$table="t_content";
$conn=new dbquery($connect, $table);
$conn_news=new dbquery($connect, 't_news');
$header_loc="Location: ".$_SERVER["HTTP_REFERER"];
define("noimage", "/images/noimage.png");


if ((isset($_POST['name'])) && ($_POST['id']=="")){
    $name=$_POST['name'];
    $text=$_POST['text'];
    $img=($_FILES['file']['name']!="")?imageUpload($_FILES['file']):noimage;
    //$logo=($_FILES['logo']['name']!="")?imageUpload($_FILES['logo']):noimage;
    //$price=$_POST['price'];
    $header=(isset($_POST['header']))?1:0;
    $footer=(isset($_POST['footer']))?1:0;
    $title=$_POST['title'];
    $description=$_POST['description'];
    $keywords=$_POST['keywords'];
    if (isset($_POST["id_pub"])){
        $public=$_POST["id_pub"];
        $field=array("name_content", "text_content", "img_content", "id_publication",  'cpu_content', 'out_header', 
            'out_footer', 'title_content','description_content','keywords_content', 'updated_content');
        $values=array($name, $text, $img, $public,  $price, translit($name), $header, $footer, $title,$description,$keywords, date("Y-m-d H:i:s"));
    } else {
        $field=array("name_content", "text_content", "img_content",  'cpu_content', 'out_header', 'out_footer', 'title_content','description_content','keywords_content', 'updated_content');
        $values=array($name, $text, $img, translit($name), $header,$footer, $title,$description,$keywords, date("Y-m-d H:i:s"));
    }
    $conn->insert($field, $values);
    header($header_loc);
}

if (isset($_POST['delete'])){ //удаление контента
    $id=$_POST["id"];
    $query=$conn->select("id_content=".$id);
    if (count($conn_news->select("id_content=".$id))>0){  //Проверка на целосстность данных в таблице новости
        echo "Удаление невозможно так как запись используется в новосях. Если вы хотите удалить запись, удалите новость<br/>"
        . "<a href='{$_SERVER["HTTP_REFERER"]}'>Вернуться</a>";
        die();
    }
    if ($query[0]['img_content']!=noimage) unlink("../..".$query[0]["img_content"]);
    if ($query[0]['logo_content']!=noimage) unlink ("../..".$query[0]["logo_content"]);
    $conn->delete("id_content=".$id);
    header($header_loc);
}

if (isset($_POST['update']) && ($_POST["update"]=="")){  //json для ajax обновления
    $id=$_POST["id"];
    $query=$conn->select("id_content=".$id);
    $row=$query["0"];
    $json=array("id"=> $row["id_content"], "name"=>$row["name_content"], "text"=>$row["text_content"], "img"=>$row["img_content"], "id_pub"=>$row["id_publication"],
        'header'=>$row['out_header'], 'footer'=>$row['out_footer'], 'cpu'=>$row['cpu_content'],
        'title'=>$row['title_content'],'description'=>$row['description_content'],'keywords'=>$row['keywords_content']);
    echo json_encode($json);    
}

if ((isset($_POST["name"])) && ($_POST["id"]!="")){ //Обновление
    $id=$_POST["id"];
    $name=$_POST["name"];
    $text=$_POST["text"];
    //$price=$_POST["price"];
    $query=$conn->select("id_content=".$id);
    $img=$query[0]['img_content'];
    //$logo=$query[0]['logo_content'];
    $header=(isset($_POST['header']))?1:0;
    $footer=(isset($_POST['footer']))?1:0;
    $title=$_POST['title'];
    $description=$_POST['description'];
    $keywords=$_POST['keywords'];
//    if (($_FILES["file"]["name"]!="")){
//        if ($query[0]['img_content']!=noimage){
//            unlink("../..".$query[0]["img_content"]);
//            $img=imageUpload($_FILES['file']);
//            if ($img==false) $img=noimage;
//        }
//    }
//        if (($_FILES["logo"]["name"]!="")){
//        if ($query[0]['logo_content']!=noimage){
//            unlink("../..".$query[0]["logo_content"]);
//            $logo=imageUpload($_FILES['logo']);
//            if ($logo==false) $logo=noimage;
//        }
//    }
    $img=($_FILES["file"]["name"]!="")?imageUpload($_FILES["file"]):$img;
    if (isset($_POST["id_pub"])){
        $public=$_POST["id_pub"];
        $field=array("name_content", "text_content", "img_content", "id_publication", "updated_content",
            'out_header', 'out_footer', 'title_content','description_content','keywords_content');
        $value=array($name, $text, $img, $public, date("Y-m-d H:i:s"),  $header, $footer,$title,$description,$keywords);
        $conn->update($field, $value, "id_content=".$id);
    }
    else {
        $field=array("name_content", "text_content", "img_content", "updated_content",  'out_header',
            'out_footer', 'title_content','description_content','keywords_content');
        $value=array($name, $text, $img, date("Y-m-d H:i:s"),  $header, $footer,$title,$description,$keywords);
        $conn->update($field, $value, "id_content=".$id);
    }
    $header=explode("?",$_SERVER["HTTP_REFERER"]);
    header("Location: {$header[0]}"."?id={$id}");
}
?>

