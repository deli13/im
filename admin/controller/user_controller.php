<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
$table='t_user';
$conn=new dbquery($connect, $table);
$header="Location: ".$_SERVER["HTTP_REFERER"];

if ((isset($_POST["name"])) && ($_POST["id"]=="")){
    $name=$_POST["name"];
    $pass=crypto($_POST["password"]);
    $role=$_POST["role"];
    $field=array("login_user", "password_user", "role");
    $values=array($name, $pass, $role);
    $conn->insert($field, $values);
    header($header);
}

if (isset($_POST["delete"])){
    $id=$_POST["id"];
    $query=$conn->select("id_user=".$id);
    if ($query[0]["role"]=="admin"){
        $result=$conn->select("role='admin'");
        if (count($result)==1){
            echo "Невозможно удалить единственного администратора<br/>"
            ."<a href='".$_SERVER["HTTP_REFERER"]."'>Вернуться</a>";
            die();
        } else $conn->delete ("id_user=".$id);
    } else $conn->delete ("id_user=".$id);
    header($header);
}

if (isset($_POST["update"]) && ($_POST["update"]=="")){
    $id=$_POST["id"];
    $query=$conn->select("id_user=".$id);
    $row=$query[0];
    $user=$row["login_user"];
    $role=$row["role"];
    $json=array("id"=>$id, "name"=>$user, "role"=>$role);
    echo json_encode($json);
}

if ((isset($_POST['name'])) && ($_POST['id']!="")){
    $id=$_POST['id'];
    $name=$_POST['name'];
    $role=$_POST['role'];
    $old_password=crypto($_POST['old-password']);
    $new_password=crypto($_POST['password']);
    $query=$conn->select("id_user=".$id." AND password_user='".$old_password."'");
    if (count($query)<1){
        echo "Неправильно введён старый пароль<br/>"
        ."<a href='".$_SERVER["HTTP_REFERER"]."'>Вернуться</a>";
        die();
    }
    $field=array("login_user", "password_user", "role");
    $value=array($name, $new_password, $role);
    $conn->update($field, $value, "id_user=".$id);
    header($header);
}
?>
