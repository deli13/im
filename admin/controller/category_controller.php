<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
$table='t_category';
$table_require='t_template';
$conn=new dbquery($connect, $table);
$header="Location: ".$_SERVER["HTTP_REFERER"];

if ((isset($_POST['name'])) && ($_POST['id']=='')){ //Добавление записи
    $name=$_POST['name'];
    $description=$_POST['description'];
    $template=$_POST['id_template'];
    $title=$_POST['title'];
    $descr_seo=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    $field=array('name_category', 'description_category', 'id_template', "title_category", "descr_category", "keywords_category");
    $values=array($name, $description, $template, $title, $descr_seo, $keywords);
    $conn->insert($field, $values);
    header($header);
}

if (isset($_POST['delete'])){ //Удаление записи
    $id=$_POST['id'];
    $conn_public=new dbquery($connect, 't_publication');
    $result=$conn_public->select('id_category='.$id);
    if (count($result)>=1){ // проверка на связанную запись перед удалением
        echo 'Невозможно удалить данную запись. Она используется в связанной таблице <br/>';
        echo '<a href="/admin/category.php"> Вернуться к странице шаблонов</a>' ;
    } else{
        $conn->delete('id_category='.$id);
        header($header);
    }
}

if (isset($_POST['update']) && ($_POST["update"]=="")){
    $id_update=$_POST['id'];
    $result=$conn->select('id_category='.$id_update);
    $id=$result[0]['id_category'];
    $name=$result[0]['name_category'];
    $description=$result[0]['description_category'];
    $id_templ=$result[0]['id_template'];
    $title=$result[0]['title_category'];
    $descr_seo=$result[0]['descr_category'];
    $keywords=$result[0]['keywords_category'];
    $jsonarr=array('id'=>$id, 'name'=>$name, 'description'=>$description, 'id_temp'=>$id_templ, 'title'=>$title, 'descr_seo'=>$descr_seo, 'keywords'=>$keywords);
    echo json_encode($jsonarr);
    
}

if ((isset($_POST['name'])) && ($_POST['id']!='')){
    $id=$_POST['id'];
    $name=$_POST['name'];
    $template=$_POST['id_template'];
    $description=$_POST['description'];
    $title=$_POST['title'];
    $descr_seo=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    $field=array("name_category", "description_category", "id_template", "updated_category",  "title_category", "descr_category", "keywords_category");
    $value=array($name, $description, $template, date("Y-m-d H:i:s"), $title, $descr_seo, $keywords);
    $conn->update($field, $value, "id_category=".$id);
    header($header);
    
}

?>
