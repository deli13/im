
<html>
    <head>
        <meta charset="utf8">
        <title>Публикация</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" href="css/bootstrap-select.min.css">
        <script   src="js/bootstrap-select.min.js" ></script>
        <!-- Подключаем TinyMCE -->
            <script src="tinymce/tinymce.min.js"></script>
            <script>tinymce.init({
            	language: 'ru', 
                force_p_newlines : false,
                forced_root_block : false,
                selector:'textarea',
                plugins: ['code autolink link table media jbimages'],
                theme_advanced_buttons1 : "jbimages",
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();

$table='t_publication';
$table_brand='t_brand';
$table_img='t_img';
$table_category='t_category';
$conn=new dbquery($connect, $table);

?>
    <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
            <div class="col-md-10">
                <nav class="navbar navbar-default">
                 <div class="collapse navbar-collapse"> 
                <form method="GET" class="navbar-form" role="search" >
                    
                    <input type="text" name="search-name" class="form-control" style="width: 60%" placeholder="Поиск">
                    
                        <select name="search-cat" class="form-control">Ыs
                                                        <option selected disabled>Выберите категорию</option>
                            <?php 
                            $conn_cat=new dbquery($connect, $table_category);
                            $query=$conn_cat->selectColumn('id_category, name_category');
                            foreach ($query as $row) {
                                echo "<option value='".$row['id_category']."'>".$row['name_category']."</option>";
                            }
                            
                            ?>
                        </select>
                    <input type="submit" name="search" class="form-control" value="Поиск">
                <?php if($_SERVER["REQUEST_URI"]!="/admin/publication.php"){ ?>
                    <a class="btn btn-default" href="publication.php">Сброс</a>
                <?php } ?>
                </form>
                </div>
                </nav>
                <div class="table_div">
                    <table class="table table-condensed">
                        <thead>
                        <th>id</th>
                        <th>Имя</th>
                        <th>Описание</th>
                        <th>Категория</th>
                        <th>CMS</th>
                        <th>Отзыв</th>
                        <th>Изображение</th>
                        <th>Цена</th>
                        <th>CPU</th>
                        <th>Создано</th>
                        <th>Изменено</th>
                        <th>Действие</th>
                        </thead>
                        <tbody>
                        <?php 
                        if (isset($_GET['search'])){
                            @$search_name=$_GET['search-name'];
                            @$search_cat=$_GET['search-cat'];
                            if ($search_name!="") $where="name_publication like '%{$search_name}%'";
                            if ($search_cat!="") $where="t_publication.id_category={$search_cat}";
                            if (($search_name!="") && ($search_cat!='')){
                            $where="name_publication like '%{$search_name}%' AND t_publication.id_category={$search_cat}";
                            }
                            
                        }
                        $join_t=array($table_brand, $table_category, $table_img);
                        $join_id=array('id_brand', 'id_category', 'id_img');
                        if ((isset($where)) && ($where!="")){
                            $result=$conn->selectJoin($join_t, $join_id, $where);
                        } else {
                            $result=$conn->selectJoin($join_t, $join_id);
                        }
                        foreach ($result as $row){
                            echo "<tr>";
                            echo "<td>{$row['id_publication']}</td>";
                            echo "<td>{$row['name_publication']}</td>";
                            echo "<td>".str200($row['description'])."</td>";
                            echo "<td>{$row['name_category']}</td>";
                            echo "<td>{$row['name_brand']}</td>";
                            echo "<td><img height=100 width=100 src='{$row['otziv_publication']}'></td>";
                            echo "<td><img height=100 width=100 src='".$row['img_desktop']."'></td>";
                            echo "<td>{$row['price']}</td>";
                            echo "<td>{$row['cpu']}</td>";
                            echo "<td>".dateNorm($row['created_publication'])."</td>";
                            echo "<td>".dateNorm($row['updated_publication'])."</td>";
                            echo "<td><form action='/admin/controller/publication_controller.php' name='delete' method='POST'>"
                            . "<input name='id' value='{$row['id_publication']}' style='display:none'>"
                            . "<input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                            . "<form name='update'>"
                            . "<input name='id' value='{$row['id_publication']}' style='display:none'>"
                            . "<input type='submit' name='update' class='btn btn-success' value='Изменить'></form></td>";
                            echo "</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                
                <p class="lead">Редактирование публикации</p>
                <div class="form-group">
                    <form action="/admin/controller/publication_controller.php" method="POST" enctype="multipart/form-data">
                        <input type="text" name="id" id="id" style="display: none" value="">
                        <input class="form-control" name="name" id="name" placeholder="Название публикации"><br/>
                        <p class="lead">Описание публикации</p>
                        <textarea class="form-control" name="descr" id="descr" rows="15" placeholder="Описание"></textarea><br/>
                        <select class="form-control " id="id_category"  required name="id_category">
                            <option selected disabled>Выберите категорию</option>
                            <?php 
                            $conn_cat=new dbquery($connect, $table_category);
                            $query=$conn_cat->selectColumn('id_category, name_category');
                            foreach ($query as $row) {
                                echo "<option value='".$row['id_category']."'>".$row['name_category']."</option>";
                            }
                            
                            ?>
                        </select><br /><br />
                        <select class="form-control " id="id_brand"  required name="id_brand">
                            <option selected disabled>Выберите CMS</option>
                            <?php 
                            $conn_brand=new dbquery($connect, $table_brand);
                            $query=$conn_brand->selectColumn('id_brand, name_brand');
                            foreach ($query as $row) {
                                echo "<option value='".$row['id_brand']."'>".$row['name_brand']."</option>";
                            }
                            unset($conn_brand);
                            ?>
                        </select><br /><br />
                        <input type="file" accept="image/*" name="file"><img src=""  id="img"><br>
                        <div class="input-group">
                            <span class="input-group-addon">Цена</span>
                            <input class="form-control" name="price" type="number" id="price">
                        </div><br/>
                        <!--SEO-->
                        <input class="form-control" type="text" name="title" id="title" placeholder="title"><br>
                        <input class="form-control" type="text" name="descr_seo" id="descr_seo" placeholder="description"><br>
                        <input class="form-control" type="text" name="keywords" id="keywords" placeholder="keywords"><br>
                        <!-- End SEO -->
                        <a class="btn btn-primary" id="content" style="display: none; margin-right: 5px" href="#" role="button">Материалы</a>
                        <a class="btn btn-primary" style="display: none" href="#" id="img-pub" role="button">Изображения</a><br/><br/>
                        <input type="submit" class="form-control btn-primary" value="Сохранить">
                    </form>
                </div>
                <div class="form-group" id="hash" style="display: none">
                    <p class="lead">Хештеги в публикации <i id="allhash"></i></p>
                    <p class="lead"> Добавление хэштегов</p>
                    <div class="input-group"> 
                    
                    <select class="form-control" name="insert_hashtag[]" id="selecthashtag" multiple title="Выберите хэштеги">
                    </select>
                    <span class="input-group-btn"><button id="submit_hashtag" class="btn btn-default">Добавить хэштеги</button></span>
                    </div><br/>
                    <div class="input-group">
                    <input type="text" class="form-control"  id="hashtag" placeholder="Новый хэштег"><br/>
                    <span class="input-group-btn"><button type="button" class="form-control btn-success" id="hashtag_append">Добавить хэштег</button></span>
                    </div>
                </div>
            </div>
        </div>
                <script>
                        $('form[name=delete]').submit(function(){
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        })
                
            var hashtag_sub=document.getElementById("submit_hashtag"); //добавление хэштегов к публикации
            hashtag_sub.addEventListener("click", function(){
                var form=document.createElement("form");
                form.setAttribute("method", "post");
                form.setAttribute("action", "/admin/controller/hashtag_controller.php");
                form.appendChild(document.getElementById("id"));
                form.appendChild(document.getElementById("selecthashtag"));
                form.submit();
            })
             
            $("#hashtag_append").click(function(){  //AJAX функция срабатывающая при отправке запроса на добавление новых хэштегов
                $.ajax({
                   type: 'post',
                   url: '/admin/controller/hashtag_controller.php',
                   dataType: 'json',
                   data: "hashtag="+$("#hashtag").val()+"&new",
                   success: function(data){
                       var req=$.parseJSON=data;
                       $("#hashtag").val("");
                       alert("Хэштег добавлен");
                       $("#selecthashtag").empty(); //Очистить select
                       req.forEach(function(item, i, req){
                       $("#selecthashtag").append($('<option value="'+item["id_hashtag"]+'">'+item["hashtag"]+'</option>'));
                       });
                       
                   }
                });
            });    
                
            $('form[name=update]').submit(function(){
            form=$(this);
            $.ajax({
                type: 'post',
                url: '/admin/controller/publication_controller.php',
                dataType: 'json',
                data: form.serialize()+'&update',
                success: function(data){
                    var req=$.parseJSON=data;
                        $('#content').css("display", "inline");
                        $('#img-pub').css("display", "inline");
                        $('#content').attr('href', '/admin/content.php?public='+req['id']+'&category='+req['category']);
                        $('#img-pub').attr('href', '/admin/image.php?public='+req['id']);
                        $('#id').val(req['id']);
                        $('#name').val(req['name']);
                        $('#description').text(req['descr']);
                        $('#id_category [value='+req["category"]+']').attr('selected', 'selected');
                        $('#id_brand [value='+req["brand"]+']').attr('selected','selected');
                        $('#img').attr('src', req['otziv']);
                        $('#price').val(req['price']);
                        $('#title').val(req['title']);
                        $('#descr_seo').val(req['descr_seo']);
                        $('#keywords').val(req['keywords']);
                    tinyMCE.activeEditor.setContent(req['descr']);
                    $("#hash").css("display", "block");
                  $.ajax({ //вывод хэштегов привязанных к публикации
                    type: "post",
                    url: '/admin/controller/hashtag_controller.php',
                    dataType: "json",
                    data: "select_hashtag&id_pub="+document.getElementById("id").value+"",
                    success: function(data){
                        var req=$.parseJSON=data;
                        var allhash=document.getElementById("allhash");
                        allhash.innerHTML=req.join(", ");
                    }
                }) //конец вложенного AJAX
                
                }
            })
  
            return false})
        
            window.onload=function(){ //AJAX при загрузке страницы
                $.ajax({ //Вывод всех доступных Хэштегов
                    type: "post",
                    url: '/admin/controller/hashtag_controller.php',
                    dataType: "json",
                    data: "select",
                    success:function(data){
                        var req=$.parseJSON=data;
                       req.forEach(function(item, i, req){
                       $("#selecthashtag").append($('<option value="'+item["id_hashtag"]+'">'+item["hashtag"]+'</option>'));
                       });
                    }
                })

            }    
        
       <?php if(isset($_GET["public"])){ //Если есть GET запрос то дублируется AJAX на вывод публикации как в случае с отправкой запроса на update
?>

    $.ajax({
                type: 'post',
                url: '/admin/controller/publication_controller.php',
                dataType: 'json',
                data: 'id= <?=$_GET["public"]?>'+'&update',
                success: function(data){
                    var req=$.parseJSON=data;
                        $('#content').css("display", "inline");
                        $('#img-pub').css("display", "inline");
                        $('#content').attr('href', '/admin/content.php?public='+req['id']+'&category='+req['category']);
                        $('#img-pub').attr('href', '/admin/image.php?public='+req['id']);
                        $('#id').val(req['id']);
                        $('#name').val(req['name']);
                        $('#description').text(req['descr']);
                        $('#id_category [value='+req["category"]+']').attr('selected', 'selected');
                        $('#id_brand [value='+req["brand"]+']').attr('selected','selected');
                        $('#img').attr('src', req['otziv']);
                        $('#price').val(req['price']);
                        $('#title').val(req['title']);
                        $('#descr_seo').val(req['descr_seo']);
                        $('#keywords').val(req['keywords']);
                    tinyMCE.activeEditor.setContent(req['descr']);
                        $("#hash").css("display", "block");
                }
            })
            $.ajax({ //вывод хэштегов привязанных к публикации
                    type: "post",
                    url: '/admin/controller/hashtag_controller.php',
                    dataType: "json",
                    data: "select_hashtag&id_pub=<?=$_GET["public"]?>",
                    success: function(data){
                        var req=$.parseJSON=data;
                        var allhash=document.getElementById("allhash");
                        allhash.innerHTML=req.join(", ");
                    }
                })
 
<?php }
?>
        </script>
    </body>
</html>