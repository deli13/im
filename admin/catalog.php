<html>
    <head>
        <meta charset="utf8">
        <title>Каталог</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="tinymce/tinymce.min.js"></script>
            <script>tinymce.init({
            	language: 'ru', 
                force_p_newlines : false,
                forced_root_block : false,
                selector:'textarea',
                plugins: ['code autolink link table media jbimages'],
                theme_advanced_buttons1 : "jbimages",
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();
$table="t_catalog";
$conn=new dbquery($connect, $table);

?>
  <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
        <div class="col-md-10">
            <div class="table_div">
                <table class="table">
                    <thead>
                    <th>ID</th>
                    <th>Название каталога</th>
                    <th>Родительский каталог</th>
                    <th>CPU</th>
                    <th>Действие</th>
                    </thead>
                    <tbody>
                    <?php
                    $query=$conn->select("");
                    foreach ($query as $val){
                        echo "<tr>";
                        echo "<td>{$val['id_catalog']}</td>";
                        echo "<td>{$val['name_catalog']}</td>";
                        $where="id_catalog='{$val['parent_catalog']}'";
                        $query_parent=$conn->select($where);
                        @$val_parent=$query_parent[0]['name_catalog'];
                        echo "<td>{$val_parent}</td>";
                        echo "<td>{$val['cpu_catalog']}</td>";
                        echo "<td><form method='POST' name='delete' action='/admin/controller/catalog_controller.php'>" //Форма удаления
                        ."<input name='id' value={$val['id_catalog']} style='display:none'>"
                        ."<input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                        ."<form name='update'>" //Форма удаления
                        ."<input name='id' value={$val['id_catalog']} style='display:none'>"
                        ."<input type='submit' name='update' class='btn btn-success' value='Изменить'></form>"
                        . "</td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div  class="form_group">
                <p class="lead">Редактирование каталога</p>
                <form action="/admin/controller/catalog_controller.php" method="POST" id="forms" name="forms" enctype="multipart/form-data">
                    <input id="id" style="display: none" name="id">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Название каталога"><br/>
                    <select name="parent" id="parent" class="form-control">
                        <option value="null" selected>Выберите родительскую категорию</option>
                        <?php 
                        $query_par=$conn->select("");
                        foreach ($query_par as $val){
                            echo "<option value='{$val['id_catalog']}'>{$val['name_catalog']}</option>";
                        }
                        ?>
                    </select><br/>
                    <textarea class="form-control" name="text" id="text" rows="15"></textarea><br/>
                    <input class="form-control" type="text" name="title" id="title" placeholder="title"><br>
                        <input class="form-control" type="text" name="descr_seo" id="descr_seo" placeholder="description"><br>
                        <input class="form-control" type="text" name="keywords" id="keywords" placeholder="keywords"><br>
                        <p class="lead">Изображение</p>
                        <input type="file" name="img" accept="image/*"/>
                        <img id="img"/>
                    <p class="lead">Характеристики каталога</p>
                    <div id="character" style="margin:15px; display:block">
                        
                    </div>
                    <button type="button" class="btn btn-success" id="add_character">Добавить характеристику</button><br/><br/>
                    <input type="submit" id="submit" class="form-control btn-primary" value="Отправить">
                </form>
            </div>
        </div>
        </div>
      <script>
          var forms=document.getElementById("forms");
          var add=document.getElementById("add_character");
          var charact=document.getElementById("character");
          
          function insertChar(val1, val2, val3){
              let divrow=document.createElement("div");
              divrow.className+="row";
              let divcol1=document.createElement("div");
              divcol1.className+="col-md-6";
              let divcol2=document.createElement("div");
              divcol2.className+="col-md-6";
              let char=document.createElement("input");
              char.className+="form-control";
              char.setAttribute("type", "text");
              char.setAttribute("name", "charact")
              char.setAttribute("placeholder", "Название характеристики")
              char.setAttribute("id",val3);
              char.value=val1;
              let edizm=document.createElement("input");
              edizm.className+=("form-control");
              edizm.setAttribute("type", "text");
              edizm.setAttribute("name", "edizm");
              edizm.setAttribute("id",val3);
              edizm.setAttribute("placeholder", "Название единицы измерения");
              edizm.value=val2;
              divcol1.appendChild(char);
              divcol2.appendChild(edizm);
              divrow.appendChild(divcol1);
              divrow.appendChild(divcol2);
              charact.appendChild(divrow);
              charact.appendChild(document.createElement("br"));
          }
          add.addEventListener("click", function(){
              insertChar("","","");
          })
          
          forms.onsubmit=function(){
              let char=document.getElementsByName("charact");
              let edizm=document.getElementsByName("edizm");
              var serializ={};
              for (i=0; i<char.length; i++){
                serializ[char[i].value]=[edizm[i].value, edizm[i].getAttribute("id").substr(2)];
              }
              let data=JSON.stringify(serializ);
              let json=document.createElement("input");
              json.setAttribute("name", "json");
              json.value=data;
              forms.appendChild(json);
          };
          
          $("form[name=update]").submit(function(){
              form=$(this);
              $.ajax({
                  type:"post",
                  url:"/admin/controller/catalog_controller.php",
                  dataType:"json",
                  data:form.serialize()+"&update",
                  success:function(data){
                    var req=$.parseJSON=data;
		    $("[name=cpu]").remove();
		    $("#character").empty();
                    $("#id").val(req['id']);
                    $("#name").val(req['name']);
                    $("#text").text(req['text']);
                    $("#parent [value="+req['parent']+"]").attr("selected", "selected");
                    $("#title").val(req['title']);
                    $("#descr_seo").val(req['description']);
                    $("#keywords").val(req['keywords']);
                    $("#img").attr("src", req['img']);
                    $("#img").attr("height", 200);
                    let cpu=document.createElement("input");
                    cpu.className+=("form-control");
                    cpu.setAttribute("name", "cpu");
                    cpu.setAttribute("placeholder", "ЧПУ")
                    cpu.value=req['cpu'];
                    document.forms.insertBefore(cpu,document.getElementById("submit"));
                    var it=req['charact'];
                    console.log(it);
                    for (var key in it){
                        insertChar(key, it[key][0], "id"+it[key][1]);
                    }
                    tinyMCE.activeEditor.setContent(req['text']);
                  }    
              })
          return false})
      </script>
  </body>
</html>