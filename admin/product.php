<html>
    <head>
        <meta charset="utf8">
        <title>Товары</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="tinymce/tinymce.min.js"></script>
            <script>tinymce.init({
            	language: 'ru', 
                force_p_newlines : false,
                forced_root_block : false,
                selector:'textarea',
                plugins: ['code autolink link table media jbimages'],
                theme_advanced_buttons1 : "jbimages",
                 toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
                toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
                    
                    });</script>
  <!-- Усё подключили -->
    </head> 
        <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();
$table="t_product";
$table_cat='t_catalog';
$table_user='t_user';
$table_service='t_service';
$conn=new dbquery($connect, $table);
$conn_cat=new dbquery($connect, $table_cat);
$conn_service=new dbquery($connect, $table_service);
$query_service=$conn_service->select("");
function optionadd($query){
    foreach ($query as $val){
        echo "<option value='{$val['id_product']}'>{$val['name_product']}</option>";
    }
}

function serviceadd($query){
    foreach ($query as $val){
        echo "<option value='{$val['id_service']}'>{$val['name_service']}</option>";
    }
}

?>
      <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
        <div class="col-md-10">
                <form method="GET" class="navbar-form" role="search">
                    <input type="text" name="search" class="form-control" style="width:60%" placeholder="Поиск">
                    <input type="submit" class="form-control" value="Найти">
                </form>
            <div class="table_div">
                <table class="table">
                    <thead>
                        <th>ID</th>
                        <th>Название</th>
                        <th>Каталог</th>
                        <th>Реальная цена</th>
                        <th>Фейковая цена</th>
                        <th>Последнее обновление</th>
                        <th>Пользователь</th>
                        <th>Действие</th>
                    </thead>
                    <?php
                    $where=(isset($_GET['search']))?"name_product like '%".filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING)."%'":"(1=1)";
                    $tab=array($table_cat, $table_user);
                    $key=array('id_catalog','id_user');
                    $query_product=$conn->selectJoin($tab,$key, $where);
                    ?>
                    <tbody>
                    <?php
                    foreach ($query_product as $val){
                        echo '<tr>';
                        echo "<td>{$val['id_product']}</td>";
                        echo "<td>{$val['name_product']}</td>";
                        echo "<td>{$val['name_catalog']}</td>";
                        echo "<td>{$val['price_product']}</td>";
                        echo "<td>{$val['fake_price_product']}</td>";
                        echo "<td>". dateNorm($val['update_product'])."</td>";
                        echo "<td>{$val['login_user']}</td>";
                        echo "<td>"
                        ."<form name='update'>" //Форма удаления
                        ."<input name='id' value={$val['id_product']} style='display:none'>"
                        ."<input type='submit' name='update' class='btn btn-success' value='Изменить'></form>"
                        . "<form method='POST' name='delete' action='/admin/controller/product_controller.php'>" //Форма удаления
                        ."<input name='id' value={$val['id_product']} style='display:none'>"
                        ."<input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                        . "</td>";
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                
                </table>
            </div>
            <div class="form-group">
                <p class="lead">Редактирование товаров</p>
                <form method="POST" id="forms" action="/admin/controller/product_controller.php" enctype="multipart/form-data">
                    <input name="id" id="id" style="display:none"/>
                    <input class="form-control" name="name" id="name" placeholder="Название"/>
                    <br/>
                    <select class="form-control" name="catalog" id="catalog">
                        <option selected disabled>Выберите каталог</option>
                        <?php
                        $query=$conn_cat->select("");
                        foreach ($query as $val){
                            echo "<option value='{$val['id_catalog']}'>{$val['name_catalog']}</option>";
                        }
                        ?>
                    </select><br/>
                    <br>
                    <p class="lead">Главный текст</p>
                    <textarea name="desk1" id="desk1" rows="15"></textarea>
                    <p class="lead">Описание</p>
                    <textarea name="desk2" id="desk2" rows="15"></textarea>
                    <p class="lead">Доставка и оплата</p>
                    <textarea name="desk3" id="desk3" rows="15"></textarea>
                    <p class="lead">Инструкции</p>
                    <textarea name="manual" id="manual" rows="15"></textarea>
                    <p class="lead">Это полезно знать</p>
                    <textarea name="desk4" id="desk4" rows="15"></textarea>
                    <br/>
                    <input class="form-control" id="link" name="link" placeholder="Ссылка на полное описание"/>
                    <br>

                    <input type="number" class="form-control" name="price" id="price" placeholder="Реальная цена"/>
                    <br>
                    <input type="number" class="form-control" name="fake" id="fake" placeholder="Фейковая цена"/>
                    <br>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"  name="main" id="main"/>
                            Выводить на главной странице
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p class="lead">Необходимые товары</p>
                            <select name="required" id="required1" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                            <select name="required" id="required2" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                            <select name="required" id="required3" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                            <select name="required" id="required4" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                            <select name="required" id="required5" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                            <select name="required" id="required6" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                        </div>
                        <div class="col-md-4">
                            <p class="lead">Персональные рекомендации</p>
                            <select name="personal" id="personal1" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                            <select name="personal" id="personal2" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                            <select name="personal" id="personal3" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                            <select name="personal" id="personal4" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                            <select name="personal" id="personal5" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                            <select name="personal" id="personal6" class="form-control">
                            <?php optionadd($query_product); ?>
                            </select><br/>
                        </div>
                        <div class="col-md-4">
                            <p class="lead">Услуги</p>
                            <select name="service" id="service1" class="form-control">
                            <?php serviceadd($query_service); ?>
                            </select><br/>
                            <select name="service" id="service2" class="form-control">
                            <?php serviceadd($query_service); ?>
                            </select><br/>
                            <select name="service" id="service3" class="form-control">
                            <?php serviceadd($query_service); ?>
                            </select><br/>
                            <select name="service" id="service4" class="form-control">
                            <?php serviceadd($query_service); ?>
                            </select><br/>
                            <select name="service" id="service5" class="form-control">
                            <?php serviceadd($query_service); ?>
                            </select><br/>
                            <select name="service" id="service6" class="form-control">
                            <?php serviceadd($query_service); ?>
                            </select><br/>
                        </div>
                    </div>
                    <br>
                    <input type="text" class="form-control" name="title" id="title" placeholder="title"/>
                    <br>
                    <input type="text" class="form-control" name="descr" id="descr" placeholder="description"/>
                    <br>
                    <input type="text" class="form-control" name="keywords" id="keywords" placeholder="keywords"/>
                    <br>
                    <p class="lead">Характеристики</p>
                    <div class="row" id="character">
                    </div>
                    <p class="lead">Изображения товара</p>
                    <input name="images[]" id="img" type="file" accept="image/*" multiple="">
                    <br>
                    <div class="row" id="img-div">
                    </div>
                    <input type="submit" class="form-control btn-primary" value="Сохранить">
                    
                </form>
            </div>
        </div>
        </div>
          <script>
          //Отбор Характеристик в зависимости от выбранного каталога
          var catalog=document.getElementById('catalog');
          catalog.addEventListener('change', function(){
              var index=this.value;
              $.ajax({
                  type: "post",
                  url: "/admin/controller/product_controller.php",
                  dataType: "json",
                  data:"character_id="+index,
                  success: function(data){
                      var req=$.parseJSON=data;
                      $("#character").empty();
                      for (var key in req){
                          $("#character").append($("<div class='row'><div class='col-md-4'><input name='id_char' style='display:none' value='"+key+"'/><label>"+req[key][0]+"</label></div>\n\
                            <div class='col-md-4'><input name='value' class='form-control'/></div>\n\
                            <div class='col-md-4'><label>"+req[key][1]+"</label></div></div><br/>"));
                      }
                  }
              })
          })
          //Конец отбора
          //Отправка формы
        var forms=document.getElementById("forms")
        forms.addEventListener('submit', function(){
        //Добавляем характеристики
            var charact_id=document.getElementsByName("id_char");
            var value_charact=document.getElementsByName('value');
            var serializ={};
            for (i=0; i<charact_id.length; i++){
                serializ[charact_id[i].value]=value_charact[i].value;
            }
            let data=JSON.stringify(serializ);
            let json_data = document.createElement("input");
            json_data.setAttribute('name', 'json');
            json_data.value=data;
        //Добавляем необходимые товары
            var required_product=document.getElementsByName('required');
            var serializ_required={};
            for (i=0; i<required_product.length; i++){
                serializ_required[i]=required_product[i].value;
            }
            data=JSON.stringify(serializ_required);
            console.log(data);
            var required_data=document.createElement("input");
            required_data.setAttribute('name', 'required_json');
            required_data.value=data;
        //Добавляем персональные рекомендации
            var personal=document.getElementsByName('personal');
            var serializ_personal={};
            for (i=0; i<personal.length; i++){
                serializ_personal[i]=personal[i].value;
            }
            data=JSON.stringify(serializ_personal);
            var personal_data=document.createElement("input");
            personal_data.setAttribute("name", "personal_json");
            personal_data.value=data;
                    //Добавляем Услуги
            var service=document.getElementsByName('service');
            var serializ_service={};
            for (i=0; i<service.length; i++){
                serializ_service[i]=service[i].value;
            }
            data=JSON.stringify(serializ_service);
            var service_data=document.createElement("input");
            service_data.setAttribute("name", "service_json");
            service_data.value=data;
            
            forms.appendChild(service_data);
            forms.appendChild(required_data);
            forms.appendChild(personal_data);
            forms.appendChild(json_data);
        });
        
        $('form[name=delete]').submit(function(){  //Подтверждение удаления
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        });
        
        function postUpdateAJAX(){  //функция для AJAX удаления
            $('button[name="img-delete"]').click(function(){//Удаление фоток
                var id_img=$(this).val();
                    $.ajax({
                        url:'/admin/controller/product_controller.php',
                        type:'post',
                        dataType:'html',
                        data:"id_img_delete="+$(this).val(),
                        success:function(data){
                            alert(data);
                            var delNode="img-"+id_img;
                            console.log(delNode);
                            var del=document.getElementById(delNode);
                            console.log(del);
                            del.parentNode.removeChild(del);
                        }
                    })
                    })
        }
        //Update
        $('form[name=update]').submit(function(){
            var form=$(this);
            $.ajax({
                type:'post',
                url: '/admin/controller/product_controller.php',
                dataType:'json',
                data: form.serialize()+"&update",
                success:function(data){
                    var req=$.parseJSON=data;
                    $("#character").empty();
                    $("#img-div").empty();
                    $("#id").val(req['id']);
                    $("#name").val(req['name']);
                    $("#desk1").text(req['desk1']);
                    $("#desk2").text(req['desk2']);
                    $("#desk3").text(req['desk3']);
                    $("#desk4").text(req['desk4']);
                    $("#manual").text(req['manual']);
                    $("#link").val(req['url']);
                    if(req['main']==1) $("#main").attr("checked", "checked");
                    $("#price").val(req['price']);
                    $("#fake").val(req['fake']);
                    $("#title").val(req['title']);
                    $("#descr").val(req['description']);
                    $("#keywords").val(req['keywords']);
                    $("#catalog [value="+req['catalog']+"]").attr("selected", "selected");
                    tinyMCE.editors[0].setContent(req['desk1']);
                    tinyMCE.editors[1].setContent(req['desk2']);
                    tinyMCE.editors[2].setContent(req['desk3']);
                    tinyMCE.editors[3].setContent(req['manual']);
                    tinyMCE.editors[4].setContent(req['desk4']);
                    
                    $("#required1 [value="+req['require'][0]+"]").attr("selected", "selected");
                    $("#required2 [value="+req['require'][1]+"]").attr("selected", "selected");
                    $("#required3 [value="+req['require'][2]+"]").attr("selected", "selected");
                    $("#required4 [value="+req['require'][3]+"]").attr("selected", "selected");
                    $("#required5 [value="+req['require'][4]+"]").attr("selected", "selected");
                    $("#required6 [value="+req['require'][5]+"]").attr("selected", "selected");
                    
                    $("#personal1 [value="+req['personal'][0]+"]").attr("selected", "selected");
                    $("#personal2 [value="+req['personal'][1]+"]").attr("selected", "selected");
                    $("#personal3 [value="+req['personal'][2]+"]").attr("selected", "selected");
                    $("#personal4 [value="+req['personal'][3]+"]").attr("selected", "selected");
                    $("#personal5 [value="+req['personal'][4]+"]").attr("selected", "selected");
                    $("#personal6 [value="+req['personal'][5]+"]").attr("selected", "selected");
//                    if(req['service']!=null){
//                    $("#service1 [value="+req['service'][0]+"]").attr("selected", "selected");
//                    $("#service2 [value="+req['service'][1]+"]").attr("selected", "selected");
//                    $("#service3 [value="+req['service'][2]+"]").attr("selected", "selected");
//                    $("#service4 [value="+req['service'][3]+"]").attr("selected", "selected");
//                    $("#service5 [value="+req['service'][4]+"]").attr("selected", "selected");
//                    $("#service6 [value="+req['service'][5]+"]").attr("selected", "selected");
//                    }
                    var char=req[0];  //Изменение характеристик
                    for(key in char){
                        var char_item=char[key];
                        $("#character").append($("<div class='row'><div class='col-md-4'><input name='id_char' style='display:none' value='"+char_item['id_character']+"'/><label>"+char_item['name_character']+"</label></div>\n\
                                <div class='col-md-4'><input name='value' class='form-control' value='"+char_item['value_character_product']+"'/></div>\n\
                                <div class='col-md-4'><label>"+char_item['ed_izm']+"</label></div></div><br/>"));
                    }
                    var img_prod=req['img'];
                    for(key1 in img_prod){ //Изображения
                        var img_item=img_prod[key1];
                        $("#img-div").append($("<div class='row' id='img-"+img_item['id_img_product']+"'><div class='col-md-6'><img src='"+img_item['img_product']+"' height=100 /></div>\n\
                    <div class='col-md-6'><button role='button' type='button' class='btn btn-danger' name='img-delete' value='"+img_item['id_img_product']+"'>Удалить</button></div>"))
                    }
                    postUpdateAJAX();
                    

                }
            })
            return false;
        })

          </script>
        <?php if((isset($_GET['id'])) && (is_numeric($_GET['id']))):?>
        <?php $request=explode("?",$_SERVER['REQUEST_URI']);
        $request=$request[0];
        $id= filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);
        ?>
          <script>
          document.addEventListener("DOMContentLoaded", function(){
          $.ajax({
                type:'post',
                url: '/admin/controller/product_controller.php',
                dataType:'json',
                data: "id=<?php echo $id ?>"+"&update",
                success:function(data){
                    var req=$.parseJSON=data;
                    $("#character").empty();
                    $("#img-div").empty();
                    $("#id").val(req['id']);
                    $("#name").val(req['name']);
                    $("#desk1").text(req['desk1']);
                    $("#desk2").text(req['desk2']);
                    $("#desk3").text(req['desk3']);
                    $("#desk4").text(req['desk4']);
                    $("#manual").text(req['manual']);
                    $("#link").val(req['url']);
                    if(req['main']==1) $("#main").attr("checked", "checked");
                    $("#price").val(req['price']);
                    $("#fake").val(req['fake']);
                    $("#title").val(req['title']);
                    $("#descr").val(req['description']);
                    $("#keywords").val(req['keywords']);
                    $("#catalog [value="+req['catalog']+"]").attr("selected", "selected");
                    
                    $("#required1 [value="+req['require'][0]+"]").attr("selected", "selected");
                    $("#required2 [value="+req['require'][1]+"]").attr("selected", "selected");
                    $("#required3 [value="+req['require'][2]+"]").attr("selected", "selected");
                    $("#required4 [value="+req['require'][3]+"]").attr("selected", "selected");
                    $("#required5 [value="+req['require'][4]+"]").attr("selected", "selected");
                    $("#required6 [value="+req['require'][5]+"]").attr("selected", "selected");
                    
                    $("#personal1 [value="+req['personal'][0]+"]").attr("selected", "selected");
                    $("#personal2 [value="+req['personal'][1]+"]").attr("selected", "selected");
                    $("#personal3 [value="+req['personal'][2]+"]").attr("selected", "selected");
                    $("#personal4 [value="+req['personal'][3]+"]").attr("selected", "selected");
                    $("#personal5 [value="+req['personal'][4]+"]").attr("selected", "selected");
                    $("#personal6 [value="+req['personal'][5]+"]").attr("selected", "selected");
//                    if(req['service']!=null){
//                    $("#service1 [value="+req['service'][0]+"]").attr("selected", "selected");
//                    $("#service2 [value="+req['service'][1]+"]").attr("selected", "selected");
//                    $("#service3 [value="+req['service'][2]+"]").attr("selected", "selected");
//                    $("#service4 [value="+req['service'][3]+"]").attr("selected", "selected");
//                    $("#service5 [value="+req['service'][4]+"]").attr("selected", "selected");
//                    $("#service6 [value="+req['service'][5]+"]").attr("selected", "selected");
//                    }
                    var char=req[0];  //Изменение характеристик
                    for(key in char){
                        var char_item=char[key];
                        $("#character").append($("<div class='row'><div class='col-md-4'><input name='id_char' style='display:none' value='"+char_item['id_character']+"'/><label>"+char_item['name_character']+"</label></div>\n\
                                <div class='col-md-4'><input name='value' class='form-control' value='"+char_item['value_character_product']+"'/></div>\n\
                                <div class='col-md-4'><label>"+char_item['ed_izm']+"</label></div></div><br/>"));
                    }
                    var img_prod=req['img'];
                    for(key1 in img_prod){ //Изображения
                        var img_item=img_prod[key1];
                        $("#img-div").append($("<div class='row' id='img-"+img_item['id_img_product']+"'><div class='col-md-6'><img src='"+img_item['img_product']+"' height=100 /></div>\n\
                    <div class='col-md-6'><button role='button' type='button' class='btn btn-danger' name='img-delete' value='"+img_item['id_img_product']+"'>Удалить</button></div>"))
                    }
                    postUpdateAJAX();
                    var btn=document.createElement("button")
                        btn.setAttribute("id", "reload");
                        btn.setAttribute("class", "btn");
                        btn.innerHTML="Добавить запись";
                        
                        document.forms[1].appendChild(btn);
                        btn.addEventListener("click", function(){console.log("click"); window.location.assign("<?php echo $request; ?>")})
                        
                    tinyMCE.editors[0].setContent(req['desk1']);
                    tinyMCE.editors[1].setContent(req['desk2']);
                    tinyMCE.editors[2].setContent(req['desk3']);
                    tinyMCE.editors[3].setContent(req['manual']);
                    tinyMCE.editors[4].setContent(req['desk4']);
                }
            })
            })
          </script>
          <?php endif ?>
      </body>
</html>